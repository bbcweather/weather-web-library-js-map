#!/bin/bash

# get current version
VERSION=$(node --eval "console.log(require('./package.json').version);")

# Build
git checkout -b build
npm test || exit 1
npm run prepublish
git add dist/map.src.js dist/map.js dist/map.css -f

# Create the bower and component files
#node_modules/copyfiles/copyfiles -u 1 build/*.json ./
node_modules/tin/bin/tin -v $VERSION
git add bower.json -f

git commit -m "Build v$VERSION"

# Tag and push
git tag v$VERSION
git push --tags --force

# Cleanup
git checkout master
git branch -D build
