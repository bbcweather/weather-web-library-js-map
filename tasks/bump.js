'use strict';

module.exports = function() {
  return {
    options: {
      files: ['package.json', 'bower.json'],
      commit: false,
      commitMessage: 'Release v%VERSION%',
      commitFiles: ['package.json', 'bower.json', 'dist/map.css', 'dist/map.js', 'dist/map.src.js'],
      createTag: false,
      tagName: 'v%VERSION',
      tagMessage: 'Version %VERSION%',
      push: false
    }
  };
};
