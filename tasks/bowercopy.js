'use strict';

module.exports = function() {
  return {
    options: {
      clean: true
    },
    js: {
      options: {
        destPrefix: '<%= config.paths.vendor_js %>'
      },
      files: {
        'leaflet': 'leaflet/dist/*.js',
        'requirejs-i18n': 'requirejs-i18n/*.js',
        'js-signals/signals.js': 'js-signals/dist/signals.min.js',
        'jasmine-signals/jasmine-signals.js': 'jasmine-signals/jasmine-signals.js',
        'weather/api.js': 'weather-web-library-js-api/dist/api.js'
      }
    },
    docs: {
      options: {
        destPrefix: '<%= config.paths.docs %>/js/vendor'
      },
      files: {
        'requirejs/require.js': 'requirejs/require.js',
        'weather/api.js': 'weather-web-library-js-api/dist/api.js',
        'matchMedia/matchMedia.js': 'matchMedia/matchMedia.js',
        'matchMedia/matchMedia.addListener.js': 'matchMedia/matchMedia.addListener.js'
      }
    },
    css: {
      options: {
        destPrefix: '<%= config.paths.vendor_css %>'
      },
      files: {
        '_leaflet.scss': 'leaflet/dist/leaflet.css'
      }
    }
  };
};
