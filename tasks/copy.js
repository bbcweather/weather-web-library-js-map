'use strict';

module.exports = function() {
  return {
    css: {
      src: '<%= config.paths.vendor_css %>/_leaflet.css',
      dest: 'dist/map.css'
    }
  };
};
