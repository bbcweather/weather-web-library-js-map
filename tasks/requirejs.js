'use strict';

module.exports = function() {
  return {
    options: {
      baseUrl: 'src/js',
      mainConfigFile: '<%= config.paths.requireConfig %>',
      name: 'weather/map/controller/responsive',
      include: ['weather/map/nls/en/strings', 'weather/map/nls/cy/strings']
    },
    source: {
      options: {
        optimize: 'none',
        paths: {
          'vendor/leaflet/leaflet': 'vendor/leaflet/leaflet-src',
          'signals': 'empty:'
        },
        out: 'dist/map.src.js'
      }
    },
    minified: {
      options: {
        optimize: 'uglify2',
        paths: {
          'signals': 'empty:'
        },
        generateSourceMaps: false,
        out: 'dist/map.js'
      }
    }
  };
};
