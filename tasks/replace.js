'use strict';

module.exports = function() {
  return {
    icons: {
      src: 'src/scss/icons/_icons-data-png.scss',
      dest: 'src/scss/icons/_icons-data-png.scss',
        replacements: [
          {
            from: '%icon-',
            to: '%icon-png-'
          }
        ]
    }
  };
};
