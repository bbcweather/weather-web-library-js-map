'use strict';

module.exports = function() {
  return {
    dist: [
      '<%= config.paths.dist %>',
      '<%= config.paths.src %>/scss/vendor',
      '<%= config.paths.src %>/scss/icons',
      '<%= config.paths.src %>/js/vendor'
    ],
    grunticon: [
      '<%= config.paths.src %>/img/icons/min',
      '<%= config.paths.src %>/scss/icons/png',
      '<%= config.paths.src %>/scss/icons/grunticon.loader.js',
      '<%= config.paths.src %>/scss/icons/_icons-fallback.scss',
      '<%= config.paths.src %>/scss/icons/preview.html'
    ]
  };
};
