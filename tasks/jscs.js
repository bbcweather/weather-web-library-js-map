'use strict';

module.exports = function() {
  return {
    options: {
      config: '.jscsrc'
    },
    main: [
      '<%= config.paths.src %>/js/**/*.js',
      '<%= config.paths.test %>**/*.js',
      '!<%= config.paths.vendor_js %>**/*.js'
    ]
  };
};
