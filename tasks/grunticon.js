'use strict';

module.exports = function() {
  return {
    icons: {
      files: [{
        expand: true,
        src: '<%= config.paths.src %>/img/icons/min/*.svg',
        dest: '<%= config.paths.src %>/scss/icons/'
      }],
      options: {
        cssprefix: '%icon-',
        datasvgcss: '_icons-data-svg.scss',
        datapngcss: '_icons-data-png.scss',
        urlpngcss: '_icons-fallback.scss'
      }
    }
  };
};
