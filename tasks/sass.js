'use strict';

module.exports = function() {

  return {
    options: {
      outputStyle: 'compressed'
    },
    dist: {
      files: {
        '<%= config.paths.dist %>/map.css': '<%= config.paths.src %>/scss/map.scss',
        '<%= config.paths.dist %>/map-ie.css': '<%= config.paths.src %>/scss/map-ie.scss'
      }
    }
  };

};
