'use strict';

module.exports = function() {
  return {
    dist: {
      files: [{
        expand: true,
        cwd: '<%= config.paths.src %>/img/icons/',
        src: ['*.svg'],
        dest: '<%= config.paths.src %>/img/icons/min/'
      }]
    }
  };
};
