'use strict';

module.exports = function() {

  return {
    scss: {
      files: ['<%= config.paths.src %>/scss/**/*.scss'],
      tasks: ['sass']
    },
    test: {
      files: [
        '<%= config.paths.src %>/**/*.js',
        '<%= config.paths.test %>/**/*.spec.js'
      ],
      tasks: ['test']
    }
  };

};
