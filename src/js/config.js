require.config({
  baseUrl: 'src/js',
  paths: {
    'weather/map': './',
    'i18n': './vendor/requirejs-i18n/i18n',
    'signals': './vendor/js-signals/signals',
    'test': '../../test/',
    'weather/api': '/js/vendor/weather/api'
  }
});
