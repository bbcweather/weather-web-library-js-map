define(function() {
  'use strict';

  return {
    hasSvgSupport: function() {
      return (typeof window.SVGRect !== 'undefined');
    },

    hasTouchSupport: function() {
      return ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
    }
  };

});
