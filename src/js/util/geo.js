define(function() {
  'use strict';

  var tileSize = 256,
      minLatitude = -85.05112878,
      maxLatitude = 85.05112878,
      minLongitude = -180,
      maxLongitude = 180;

  function _clip(value, minValue, maxValue) {
    return Math.min(Math.max(value, minValue), maxValue);
  }

  function _mapSizeForZoomLevel(zoomLevel) {
    return tileSize << zoomLevel;
  }

  function latitudeLongitudeToPixelXY(latitude, longitude, zoom) {
    var x, y, sinLatitude, mapSize, pixelX, pixelY;

    latitude = _clip(latitude, minLatitude, maxLatitude);
    longitude = _clip(longitude, minLongitude, maxLongitude);

    x = (longitude + 180) / 360;
    sinLatitude = Math.sin(latitude * Math.PI / 180);
    y = 0.5 - Math.log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI);

    mapSize = _mapSizeForZoomLevel(zoom);
    pixelX = _clip(x * mapSize + 0.5, 0, mapSize - 1);
    pixelY = _clip(y * mapSize + 0.5, 0, mapSize - 1);

    return {
      x: pixelX,
      y: pixelY
    };
  }

  function pixelXYToTileXY(pixelX, pixelY) {
    var tileX, tileY;

    tileX = pixelX / tileSize;
    tileY = pixelY / tileSize;

    return {
      x: tileX,
      y: tileY
    };
  }

  function quadKeyFromXYZ(tileX, tileY, zoom, invertY) {
    var quadKey;

    quadKey = [];

    // Some coordinate systems invert the Y axis (row 0 is at the bottom):
    if (invertY) {
      tileY = (Math.pow(2, zoom) - 1) - tileY;
    }

    for (var i = zoom; i > 0; i--) {
      var digit, mask;

      digit = 0;
      mask = 1 << (i - 1);

      if ((tileX & mask) !== 0) {
        digit += 1;
      }

      if ((tileY & mask) !== 0) {
        digit += 2;
      }

      quadKey.push(digit);

    }

    return quadKey.join('');
  }

  return {
    latitudeLongitudeToPixelXY: latitudeLongitudeToPixelXY,
    pixelXYToTileXY: pixelXYToTileXY,
    quadKeyFromXYZ: quadKeyFromXYZ
  };

});
