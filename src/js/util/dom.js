define(function() {

  var dom = {

    create: function(tag, className, container) {
      var element = document.createElement(tag);

      if (className) {
        element.className = className;
      }

      if (container) {
        container.appendChild(element);
      }

      return element;
    },

    hasClass: function(element, name) {
      return ((' ' + element.className + ' ').indexOf(' ' + name + ' ') > -1);
    },

    addClass: function(element, name) {
      if (!dom.hasClass(element, name)) {
        element.className += (element.className ? ' ' : '') + name;
      }
    },

    removeClass: function(element, name) {
      element.className = element.className.replace(
        new RegExp('(^| )' + name + '( |$)', 'g'), ' '
      ).replace(/(^ | $)/g, '');
    }

  };

  return dom;

});
