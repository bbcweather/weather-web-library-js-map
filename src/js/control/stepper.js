define([
  'weather/map/control/control',
  'i18n!weather/map/nls/strings',
  'weather/map/util/dom',
  'signals'
], function(Control, strings, dom, signals) {
  'use strict';

  var Stepper,
      keyCodes;

  keyCodes = {
    left:  37,
    right: 39
  };

  Stepper = function() {
    this.hasFocus = false;
    this.events = {
      'next': new signals.Signal(),
      'previous': new signals.Signal()
    };
  };

  Stepper.prototype = new Control();
  Stepper.prototype.constructor = Stepper;

  Stepper.prototype.onAdd = function() {
    var element,
        previousButton,
        nextButton,
        focusListener,
        blurListener,
        self;

    self = this;

    element = dom.create('div', 'stepper');

    previousButton = dom.create('button', 'stepper__button', element);
    dom.addClass(previousButton, 'stepper__button--previous');
    previousButton.innerHTML = '<span class="stepper__button-icon">' + strings.previous + '</span>';

    nextButton = dom.create('button', 'stepper__button', element);
    dom.addClass(nextButton, 'stepper__button--next');
    nextButton.innerHTML = '<span class="stepper__button-icon">' + strings.next + '</span>';

    previousButton.addEventListener('click', function() {
      self.events.previous.dispatch();
    });

    nextButton.addEventListener('click', function() {
      self.events.next.dispatch();
    });

    element.addEventListener('keydown', function(event) {
      var key = event.which || event.keyCode;

      if (key === keyCodes.left && self.hasPrevious()) {
        self.previousButton.focus();
        self.events.previous.dispatch();

      } else if (key === keyCodes.right && self.hasNext()) {
        self.nextButton.focus();
        self.events.next.dispatch();
      }
    });

    focusListener = function() {
      self.hasFocus = true;
    };

    blurListener = function() {
      self.hasFocus = false;
    };

    nextButton.addEventListener('focus', focusListener);
    nextButton.addEventListener('blur', blurListener);
    previousButton.addEventListener('focus', focusListener);
    previousButton.addEventListener('blur', focusListener);

    // Prevent the buttons from staying in a highlighted state after
    // clicking either the previous or next button
    element.addEventListener('mouseout', function() {
      if (self.hasFocus) {
        document.activeElement.blur();
      }
    });

    this.nextButton = nextButton;
    this.previousButton = previousButton;

    return element;

  };

  Stepper.prototype.hasNext = function(hasNext) {

    if (hasNext !== undefined) {
      if (hasNext) {
        this.nextButton.disabled = false;
      } else {
        if (this.hasFocus) {
          this.previousButton.focus();
        }
        this.nextButton.disabled = true;
      }
    }

    return !this.nextButton.disabled;
  };

  Stepper.prototype.hasPrevious = function(hasPrevious) {

    if (hasPrevious !== undefined) {
      if (hasPrevious) {
        this.previousButton.disabled = false;
      } else {
        if (this.hasFocus) {
          this.nextButton.focus();
        }
        this.previousButton.disabled = true;
      }
    }

    return !this.previousButton.disabled;
  };

  return Stepper;

});
