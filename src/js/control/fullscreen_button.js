define([
  'weather/map/control/control',
  'i18n!weather/map/nls/strings',
  'weather/map/util/dom',
  'signals'
], function(Control, strings, dom, signals) {
  'use strict';

  var FullscreenButton;

  FullscreenButton = function() {
    this.events = {
      click: new signals.Signal()
    };
  };

  FullscreenButton.prototype = new Control();
  FullscreenButton.prototype.constructor = FullscreenButton;

  FullscreenButton.prototype.focus = function() {
    this.element.focus();
  };

  FullscreenButton.prototype.onAdd = function() {
    var self = this;

    this.element = dom.create('button', 'fullscreen-button');
    this.element.innerHTML = '<span class="fullscreen-button__icon"></span>' +
        '<span class="fullscreen-button__label">' +
        strings.open_map + '</span>';

    this.element.addEventListener('click', function() {
      self.events.click.dispatch();
    });

    return this.element;
  };

  return FullscreenButton;

});
