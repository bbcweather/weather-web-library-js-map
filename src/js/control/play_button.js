define([
  'weather/map/control/control',
  'i18n!weather/map/nls/strings',
  'signals',
  'weather/map/util/dom'
], function(Control, strings, signals, dom) {
  'use strict';

  var PlayButton,
      pauseClass = 'play-button--pause',
      replayClass = 'play-button--replay';

  PlayButton = function() {
    this.events = {
      playClick: new signals.Signal(),
      pauseClick: new signals.Signal(),
      replayClick: new signals.Signal()
    };

    this._states = {
      ready: 'Ready',
      playing: 'Playing',
      finished: 'Finished'
    };

    this._currentState = this._states.ready;
  };

  PlayButton.prototype = new Control();
  PlayButton.prototype.constructor = PlayButton;

  PlayButton.prototype.onAdd = function() {
    var self = this;

    this.element = dom.create('button', 'play-button');
    this.playLabel = dom.create('span', 'play-button__label');
    this.playLabel.innerHTML = strings.play;

    this.element.appendChild(this.playLabel);

    this.element.addEventListener('click', function() {
      if (self._currentState === self._states.ready) {
        self._showPause();
        self.events.playClick.dispatch();
      } else if (self._currentState === self._states.playing) {
        self.showPlay();
        self.events.pauseClick.dispatch();
      } else if (self._currentState === self._states.finished) {
        self._showPause();
        self.events.replayClick.dispatch();
      }
    });

    return this.element;
  };

  PlayButton.prototype.showPlay = function() {
    this._currentState = this._states.ready;
    _resetIcon(this);
    this.playLabel.innerHTML = strings.play;
  };

  PlayButton.prototype.showReplay = function() {
    this._currentState = this._states.finished;
    _resetIcon(this);
    dom.addClass(this.element, replayClass);
    this.playLabel.innerHTML = strings.replay;
  };

  PlayButton.prototype._showPause = function() {
    this._currentState = this._states.playing;
    _resetIcon(this);
    dom.addClass(this.element, pauseClass);
    this.playLabel.innerHTML = strings.pause;
  };

  function _resetIcon(self) {
    dom.removeClass(self.element, replayClass);
    dom.removeClass(self.element, pauseClass);
  }

  return PlayButton;

});
