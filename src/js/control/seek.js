define([
  'weather/map/control/control',
  'i18n!weather/map/nls/strings',
  'weather/map/util/dom',
  'weather/map/util/browser',
  'signals'
], function(Control, strings, dom, browser, signals) {
  'use strict';

  var Seek;

  Seek = function(options) {
    this.events = {
      seeking: new signals.Signal()
    };
    if (options.duration === undefined) {
      throw new Error('Seek requires duration to be passed in options');
    } else {
      this.duration = options.duration;
    }
    this.markers = options.markers || [];
  };

  Seek.prototype = new Control();
  Seek.prototype.constructor = Seek;

  Seek.prototype.onAdd = function() {
    var container,
        self,
        i,
        markersContainer,
        marker,
        markerElement;

    self = this;

    container = dom.create('div', 'seek');

    markersContainer = dom.create('div', 'seek__container', container);

    for (i = 0; i < this.markers.length; i++) {
      marker = this.markers[i];

      markerElement = dom.create('div', 'seek__marker', markersContainer);
      if (marker.label !== undefined) {
        dom.addClass(markerElement, 'seek__marker--with-label');
        markerElement.innerHTML = strings[marker.label.toLowerCase()];
      }
      markerElement.style.left = (100 * (this.markers[i].position / this.duration)) + '%';
    }

    this.seekBar = dom.create('div', 'seek__seek-bar', markersContainer);
    this.timePassed = dom.create('div', 'seek__time-passed', this.seekBar);
    dom.create('div', 'seek__slider', this.seekBar);

    container.addEventListener('mousedown', function(event) {
      _handleSliderEvent(self, event);
    });

    document.addEventListener('mousemove', function(event) {
      if (self._isDragging) {
        _handleSliderEvent(self, event);
      }
    });

    document.addEventListener('mouseup', function() {
      self._isDragging = false;
    });

    if (browser.hasTouchSupport()) {

      container.addEventListener('touchstart', function(event) {
        _handleSliderEvent(self, event);
      });

      document.addEventListener('touchmove', function(event) {
        if (self._isDragging) {
          _handleSliderEvent(self, event);
        }
      });

      document.addEventListener('touchcancel', function() {
        self._isDragging = false;
      });

      document.addEventListener('touchend', function() {
        self._isDragging = false;
      });
    }

    container.addEventListener('selectstart', function(event) {
      event.preventDefault();
    });

    return container;

  };

  Seek.prototype.setPosition = function(position) {
    var sliderXFraction = position / this.duration;
    _moveSlider(this, sliderXFraction);
  };

  function _handleSliderEvent(self, event) {
    var viewportX,
        sliderXFraction,
        position;

    self._isDragging = true;

    viewportX = event.clientX || (event.touches && event.touches[0].pageX) || 0;
    sliderXFraction = (viewportX - self.seekBar.getBoundingClientRect().left) / self.seekBar.offsetWidth;
    sliderXFraction = _moveSlider(self, sliderXFraction);
    position = sliderXFraction * self.duration;
    if (position !== self._currentPosition) {
      self.events.seeking.dispatch(position);
      self._currentPosition = position;
    }
  }

  function _moveSlider(self, sliderXFraction) {
    var sliderPercent;

    if (sliderXFraction < 0) {
      sliderXFraction = 0;
    } else if (sliderXFraction > 1) {
      sliderXFraction = 1;
    }

    sliderPercent = (sliderXFraction * 100) + '%';
    self.timePassed.style.width = sliderPercent;
    return sliderXFraction;
  }

  return Seek;

});
