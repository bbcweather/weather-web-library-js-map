define([
  'weather/map/control/control',
  'weather/map/util/dom'
], function(Control, dom) {

  var Filler;

  Filler = function() {};

  Filler.prototype = new Control();
  Filler.prototype.constructor = Filler;

  Filler.prototype.onAdd = function() {
    return dom.create('div', 'filler');
  };

  return Filler;

});
