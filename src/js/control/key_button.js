define([
  'weather/map/control/control',
  'i18n!weather/map/nls/strings',
  'weather/map/util/dom',
  'signals'
], function(Control, strings, dom, signals) {
  'use strict';

  var KeyButton;

  KeyButton = function() {
    this.events = {
      click: new signals.Signal()
    };
  };

  KeyButton.prototype = new Control();
  KeyButton.prototype.constructor = KeyButton;

  KeyButton.prototype.focus = function() {
    this.element.focus();
  };

  KeyButton.prototype.onAdd = function() {
    var self = this;

    this.element = dom.create('button', 'key-button');

    this.element.innerHTML = '<span class="key-button__label">' +
      strings.open_key + '</span>';

    this.element.addEventListener('click', function() {
      self.events.click.dispatch();
    });

    return this.element;
  };

  return KeyButton;

});
