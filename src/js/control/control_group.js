define([
  'weather/map/control/control',
  'weather/map/util/dom'
], function(Control, dom) {

  var ControlGroup;

  ControlGroup = function(options) {
    options = options || {};
    this.name = options.name;
    this.controls = options.controls || [];
  };

  ControlGroup.prototype = new Control();
  ControlGroup.prototype.constructor = ControlGroup;

  ControlGroup.prototype.onAdd = function() {
    var element,
        controlElement,
        i;

    element = dom.create('div', 'control-group');
    dom.addClass(element, 'control-group--' + this.name);

    for (i = 0; i < this.controls.length; i++) {
      controlElement = this.controls[i].onAdd();
      element.appendChild(controlElement);
    }

    return element;
  };

  return ControlGroup;

});
