define([
  'weather/map/control/control',
  'i18n!weather/map/nls/strings',
  'weather/map/util/dom',
  'signals'
], function(Control, strings, dom, signals) {
  'use strict';

  var CloseButton;

  CloseButton = function() {
    this.events = {
      click: new signals.Signal()
    };
  };

  CloseButton.prototype = new Control();
  CloseButton.prototype.constructor = CloseButton;

  CloseButton.prototype.focus = function() {
    this.element.focus();
  };

  CloseButton.prototype.onAdd = function() {
    var self = this;

    this.element = dom.create('button', 'close-button');
    this.element.innerHTML = '<span class="close-button__label">' +
      strings.close_map + '</span>';

    this.element.addEventListener('click', function() {
      self.events.click.dispatch();
    });

    return this.element;
  };

  return CloseButton;

});
