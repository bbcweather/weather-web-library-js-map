define([
  'i18n!weather/map/nls/strings',
  'weather/map/util/dom',
  'weather/map/control/control'
], function(strings, dom, Control) {
  'use strict';

  var DayTime = function(options) {
    this.options = options;
  };

  DayTime.prototype = new Control();

  DayTime.prototype.onAdd = function() {
    this.container = dom.create('div', 'day-time day-time--hidden');

    this.dayLabel = dom.create('div', 'day-time__label day-time__label--day');
    this.timeLabel = dom.create('div', 'day-time__label');

    this.container.appendChild(this.dayLabel);
    this.container.appendChild(this.timeLabel);

    if (this.options) {
      this.updateDateTime(this.options);
    }

    return this.container;
  };

  /**
   * Update the labels with the new time and makes it visible if we now have a day
   * @param {Object} [options] - Should contain time and/or day properties
   */
  DayTime.prototype.updateDateTime = function(options) {
    if (options.day) {
      this.dayLabel.innerHTML = strings[options.day.toLowerCase()];
    }

    if (options.time) {
      if (options.time.toString().toLowerCase() === 'day') {
        dom.addClass(this.timeLabel, 'day-time__label--hidden');
        this.timeLabel.innerHTML = '';
      } else {
        this.timeLabel.innerHTML = options.time;
        dom.removeClass(this.timeLabel, 'day-time__label--hidden');
      }
    }

    if (this.dayLabel.innerHTML !== '') {
      dom.removeClass(this.container, 'day-time--hidden');
    }

  };

  return DayTime;

});
