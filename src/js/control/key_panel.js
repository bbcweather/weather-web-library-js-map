define([
  'weather/map/control/control',
  'i18n!weather/map/nls/strings',
  'weather/map/util/dom',
  'signals'
], function(Control, strings, dom, signals) {
  'use strict';

  var Key,
      openClass = 'key-panel--open';

  Key = function() {
    this.events = {
      open: new signals.Signal(),
      close: new signals.Signal()
    };
  };

  Key.prototype = new Control();

  Key.prototype.onAdd = function() {
    var self,
        element,
        closeButton,
        keyHeading,
        innerKeyEl;

    self = this;

    element = dom.create('div', 'key-panel');

    closeButton = dom.create('button', 'key-panel__close-button', element);
    closeButton.innerHTML = '<span class="key-panel__close-label">' +
        strings.close_key + '</span>';

    keyHeading = dom.create('h2', 'key-panel__title', element);
    keyHeading.innerHTML = strings.key;

    innerKeyEl = dom.create('div', 'key-panel__container', element);
    innerKeyEl.innerHTML =
      '<div class="key-panel__row">' +
      '  <div class="key-panel__label key-panel__row__el">' + strings.cloud + '</div>' +
      '  <div class="key-panel__indicator key-panel__row__el">' + strings.clear + '</div>' +
      '  <div class="key-panel__row__el">' +
      '    <div class="key-panel__gradient key-panel__gradient--cloud-land">' + strings.land + '</div>' +
      '    <div class="key-panel__gradient key-panel__gradient--cloud-sea">' + strings.sea + '</div>' +
      '  </div>' +
      '  <div class="key-panel__indicator key-panel__row__el">' + strings.cloudy + '</div>' +
      '</div>' +
      '<div class="key-panel__row">' +
      '  <div class="key-panel__label key-panel__row__el">' + strings.rainfall + '</div>' +
      '  <div class="key-panel__indicator key-panel__row__el">' + strings.light + '</div>' +
      '  <div class="key-panel__gradient key-panel__gradient--rainfall">' +
      '    <span class="key-panel__gradient__heavy-label">' + strings.heavy + '</span>' +
      '  </div>' +
      '  <div class="key-panel__indicator key-panel__row__el">' + strings.extreme + '</div>' +
      '</div>' +
      '<div class="key-panel__row">' +
      '  <div class="key-panel__label key-panel__row__el">' + strings.snowfall + '</div>' +
      '  <div class="key-panel__indicator key-panel__row__el">' + strings.light + '</div>' +
      '  <div class="key-panel__gradient key-panel__gradient--snowfall">&nbsp;</div>' +
      '  <div class="key-panel__indicator key-panel__row__el">' + strings.heavy + '</div>' +
      '</div>' +
      '<div class="key-panel__row">' +
      '  <div class="key-panel__label key-panel__row__el">' + strings.fog + '</div>' +
      '  <div class="key-panel__indicator key-panel__row__el">' + strings.light + '</div>' +
      '  <div class="key-panel__gradient key-panel__gradient--fog">&nbsp;</div>' +
      '  <div class="key-panel__indicator key-panel__row__el">' + strings.heavy + '</div>' +
      '</div>' +
      '<div class="key-panel__row">' +
      '  <div class="key-panel__label key-panel__row__el">' + strings.frost + '</div>' +
      '  <div class="key-panel__indicator key-panel__row__el">' + strings.light + '</div>' +
      '  <div class="key-panel__gradient key-panel__gradient--frost">&nbsp;</div>' +
      '  <div class="key-panel__indicator key-panel__row__el">' + strings.heavy + '</div>' +
      '</div>';

    closeButton.addEventListener('click', function() {
      self.close();
    });

    this.element = element;
    this.closeButton = closeButton;

    return element;
  };

  Key.prototype.focus = function() {
    this.closeButton.focus();
  };

  Key.prototype.open = function() {
    if (!dom.hasClass(this.element, openClass)) {
      dom.addClass(this.element, openClass);
      this.events.open.dispatch();
    }
  };

  Key.prototype.close = function() {
    if (dom.hasClass(this.element, openClass)) {
      dom.removeClass(this.element, openClass);
      this.events.close.dispatch();
    }
  };

  return Key;

});
