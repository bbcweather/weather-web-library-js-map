define([
  'signals'
], function(signals) {

  var Timeslots;

  Timeslots = function(hourlyTimeslots, dailyTimeslots) {
    this._hourlyTimeslots = hourlyTimeslots;
    this._dailyTimeslots = dailyTimeslots;
    this._timeslots = hourlyTimeslots.concat(dailyTimeslots);
    this._firstIndex = 0;
    this._position = 0;
    this._isPlaying = false;
    for (var i = 0; i < this._timeslots.length; i++) {
      if (this._timeslots[i].isInitial) {
        this._firstIndex = i;
        break;
      }
    }
    this._index = this._firstIndex;

    this.events = {
      timeslotChanged: new signals.Signal(),
      positionChanged: new signals.Signal(),
      paused: new signals.Signal()
    };
  };

  Timeslots.prototype.first = function() {
    this._index = this._firstIndex;
    _changePosition(this);
    this.events.positionChanged.dispatch(this._position);
  };

  Timeslots.prototype.next = function() {
    if (this._index < this._timeslots.length - 1) {
      this._index++;
      _changePosition(this);
      this.events.positionChanged.dispatch(this._position);
    }
  };

  Timeslots.prototype.previous = function() {
    if (this._index > this._firstIndex) {
      this._index--;
      _changePosition(this);
      this.events.positionChanged.dispatch(this._position);
    }
  };

  Timeslots.prototype.isAtStart = function() {
    return (this._index === this._firstIndex);
  };

  Timeslots.prototype.isAtEnd = function() {
    return (this._index === this._timeslots.length - 1);
  };

  Timeslots.prototype.getCurrentTimeslot = function() {
    return this._timeslots[this._index];
  };

  Timeslots.prototype.goToTimeslotAtPosition = function(position) {
    var timeslotIndex;

    if ((position + this._hourlyTimeslots[this._firstIndex].dateTime) <= this._dailyTimeslots[0].dateTime) {
      timeslotIndex = Math.floor(position / (10800000)) + this._firstIndex;
    } else {
      timeslotIndex = Math.floor((position + this._hourlyTimeslots[this._firstIndex].dateTime - this._dailyTimeslots[0].dateTime) / (43200000)) + this._hourlyTimeslots.length;
    }

    this._position = position;

    if (timeslotIndex !== this._index) {
      this._index = timeslotIndex;
      _changeTimeslot(this);
    }
  };

  Timeslots.prototype.getMarkers = function() {
    var thisDay, i, marker;
    thisDay = '';
    this.markers = [];
    for (i = this._firstIndex; i < this._timeslots.length; i++) {
      marker = {};
      marker.position = this._timeslots[i].dateTime - this._timeslots[this._firstIndex].dateTime;
      if (this._timeslots[i].dayShort !== thisDay) {
        thisDay = this._timeslots[i].dayShort;
        marker.label = thisDay;
      }
      this.markers.push(marker);
    }
    return this.markers;
  };

  Timeslots.prototype.getDuration = function() {
    return this._timeslots[this._timeslots.length - 1].dateTime - this._timeslots[this._firstIndex].dateTime;
  };

  Timeslots.prototype.play = function() {
    var self = this;
    this._isPlaying = true;
    _keepPlaying(self);
  };

  Timeslots.prototype.pause = function() {
    this._isPlaying = false;
    this.events.paused.dispatch();
  };

  Timeslots.prototype.isPlaying = function() {
    return this._isPlaying;
  };

  function _keepPlaying(self) {
    if (self._isPlaying) {
      if (self.isAtEnd()) {
        self.pause();
      } else {
        setTimeout(function() {
          _keepPlaying(self);
        }, 20); //50fps
      }
      self._position += 108000; // 3 hours every 2 seconds at 50fps
      self.goToTimeslotAtPosition(self._position);
      self.events.positionChanged.dispatch(self._position);
    }
  }

  function _changeTimeslot(self) {
    self.events.timeslotChanged.dispatch(self._timeslots[self._index]);
  }

  function _changePosition(self) {
    if (self.markers) {
      self._position = self.markers[self._index - self._firstIndex].position;
    }
    _changeTimeslot(self);
  }

  return Timeslots;

});
