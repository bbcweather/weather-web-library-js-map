define([
  'vendor/leaflet/leaflet',
  'weather/map/util/dom',
  'weather/map/util/browser',
  'signals'
], function(leaflet, dom, browser, signals) {
  'use strict';

  var Map;

  var defaultOptions = {
    'zoom': 9,
    'center': [51.505, -0.09]
  };

  var ukBounds = leaflet.latLngBounds(
    leaflet.latLng(61.603, -10.854),
    leaflet.latLng(49.022, 2.7125)
  );

  var defaultHandlers = [
    'dragging',
    'keyboard',
    'tap'
  ];

  Map = function(options) {
    var self = this;

    options = options || {};
    if (options.container === undefined) {
      throw new Error('Map requires container to be passed in options');
    } else {
      this.container = options.container;
    }

    this.center = options.center || defaultOptions.center;
    this.zoom = options.zoom || defaultOptions.zoom;

    dom.addClass(this.container, 'weather-map');
    dom.addClass(this.container, 'weather-map--loaded');

    if (!browser.hasSvgSupport()) {
      dom.addClass(this.container, 'weather-map--no-svg');
    }

    this.mapContainer = dom.create('div', 'weather-map__map', this.container);
    this.mapContainer.id = 'leaflet-map';

    this.engine = leaflet.map(this.mapContainer.id, {
      center: this.center,
      zoom: this.zoom,
      minZoom: 9,
      maxZoom: 9,
      maxBounds: ukBounds,
      zoomControl: false,
      scrollWheelZoom: false,
      touchZoom: false,
      doubleClickZoom: false,
      boxZoom: false,
      attributionControl: false,
      keyboard: false
    });

    this.controls = [];
    this.controlsContainer = dom.create('div', 'weather-map__controls', this.container);

    this.events = {
      active: new signals.Signal(),
      inactive: new signals.Signal(),
      moveEnd: new signals.Signal()
    };

    this.engine.on('moveend', function() {
      self.events.moveEnd.dispatch();
    });

    this.engine.on('dragstart', function() {
      dom.addClass(self.container, 'weather-map--dragging');
    });

    this.engine.on('dragend', function() {
      dom.removeClass(self.container, 'weather-map--dragging');
    });
  };

  Map.prototype.addControl = function(control) {
    var element;

    element = control.onAdd();
    this.controls.push(control);
    this.controlsContainer.appendChild(element);
  };

  Map.prototype.hasControl = function(control) {
    return this.controls.indexOf(control) !== -1;
  };

  Map.prototype.addLayer = function(layer) {
    this.engine.addLayer(layer);
  };

  Map.prototype.removeLayer = function(layer) {
    this.engine.removeLayer(layer);
  };

  Map.prototype.getBounds = function() {
    return this.engine.getBounds();
  };

  Map.prototype.getMaxBounds = function() {
    return ukBounds;
  };

  Map.prototype.getZoom = function() {
    return this.engine.getZoom();
  };

  Map.prototype.addMarker = function(marker) {
    leaflet.marker(marker.latlng, {
      icon: marker.onAdd(this.container),
      keyboard: false
    }).addTo(this.engine);
  };

  Map.prototype.invalidateSize = function() {
    this.engine.invalidateSize();
  };

  Map.prototype.reset = function() {
    this.engine.setView(this.center, this.zoom, true);
  };

  Map.prototype.activate = function() {

    for (var h in defaultHandlers) {
      if (defaultHandlers.hasOwnProperty(h)) {
        var handler = defaultHandlers[h];
        if (this.engine[handler] instanceof leaflet.Handler) {
          this.engine[handler].enable();
        }
      }
    }

    dom.addClass(this.container, 'weather-map--active');
    this.mapContainer.tabIndex = 0;
    this.events.active.dispatch();

  };

  Map.prototype.deactivate = function() {

    for (var h in defaultHandlers) {
      if (defaultHandlers.hasOwnProperty(h)) {
        var handler = defaultHandlers[h];
        if (this.engine[handler] instanceof leaflet.Handler) {
          this.engine[handler].disable();
        }
      }
    }

    dom.removeClass(this.container, 'weather-map--active');
    this.mapContainer.tabIndex = -1;
    this.events.inactive.dispatch();

  };

  return Map;

});
