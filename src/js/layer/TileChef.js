define([
  'vendor/leaflet/leaflet',
  'util/geo'
], function(leaflet, geo) {

  leaflet.TileLayer.Tilechef = leaflet.TileLayer.extend({

    options: {
      subdomains: ['tile1', 'tile2', 'tile3', 'tile4'],
      minZoom: 7,
      maxNativeZoom: 10
    },

    initialize: function(url, options) {
      this._url = url;
      options = leaflet.setOptions(this, options);

      // Adds an empty alt attribute to the tile image markup to prevent
      // voiceover from reading out the tile names.
      this.on('tileload', function(tileEvent) {
        tileEvent.tile.setAttribute('alt', '');
      });
    },

    getTileUrl: function(coords) {
      var quadKey,
          maxSourceLength,
          source,
          recipe;

      quadKey = geo.quadKeyFromXYZ(coords.x, coords.y, this._getZoomForUrl());

      maxSourceLength = 7;
      recipe = (recipe = quadKey.slice(maxSourceLength)) === '' ? '-' : recipe;
      source = quadKey.slice(0, maxSourceLength);

      // UK Bounds = x 60-64, y 36-43
      // Europe Bounds = x 14-17, y 8-11

      return leaflet.Util.template(this._url, leaflet.extend({
        s: this._getSubdomain(coords),
        recipe: recipe,
        source: source
      }, this.options));

    }

  });

  leaflet.tileLayer.tilechef = function(url, options) {
    return new leaflet.TileLayer.Tilechef(url, options);
  };

  return leaflet.TileLayer.Tilechef;

});
