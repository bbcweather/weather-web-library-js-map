define([
  'weather/map/marker/location',
  'weather/map/util/geo',
  'signals'
], function(LocationMarker, geo, signals) {
  'use strict';

  var Locations;

  function _quadKeysWithinBounds(bounds, zoom) {
    var north, south, east, west, minPixel, maxPixel, minTile, maxTile,
        x, y, quadKeys, quadKey;

    north = bounds.getNorth();
    west = bounds.getWest();
    south = bounds.getSouth();
    east = bounds.getEast();

    minPixel = geo.latitudeLongitudeToPixelXY(north, west, zoom);
    maxPixel = geo.latitudeLongitudeToPixelXY(south, east, zoom);

    minTile = geo.pixelXYToTileXY(minPixel.x, minPixel.y);
    maxTile = geo.pixelXYToTileXY(maxPixel.x, maxPixel.y);

    quadKeys = [];

    for (x = minTile.x; x <= maxTile.x; x++) {
      for (y = minTile.y; y <= maxTile.y; y++) {
        quadKey = geo.quadKeyFromXYZ(x, y, zoom);
        quadKeys.push(quadKey);
      }
    }

    return quadKeys;
  }

  function _fetchLocations(self, quadKey) {
    self.api.map.locationsFromQuadKey(quadKey).then(function(locations) {
      for (var i = 0; i < locations.length; i++) {
        _parseLocation(self, locations[i]);
      }
    });
  }

  function _fetchActiveLocation(self) {
    self.api.map.locationsFromLocationId(self.activeLocation).then(function(locations) {
      _parseLocation(self, locations[0]);
    });
  }

  function _parseLocation(self, location) {
    if (!self.seenLocationIds[location.id]) {
      var locationMarker = new LocationMarker(
        location,
        {
          timeslot: self.timeslot.utc,
          temperatureUnit: self.temperatureUnit
        }
      );
      self.seenLocationIds[location.id] = true;
      self.locationMarkers.push(locationMarker);
      self.events.locationAdded.dispatch(locationMarker);
    }
  }

  Locations = function(options) {
    this.api = options.api;
    this.timeslot = options.timeslot;
    this.activeLocation = options.activeLocation;
    this.temperatureUnit = options.temperatureUnit;
    this.seenQuadKeys = [];
    this.seenLocationIds = [];
    this.locationMarkers = [];
    this.events = {
      locationAdded: new signals.Signal()
    };
    _fetchActiveLocation(this);
  };

  Locations.prototype.processBoundsChange = function(bounds, zoom) {
    var quadKeys, quadKey, i;

    quadKeys = _quadKeysWithinBounds(bounds, zoom);

    for (i = 0; i < quadKeys.length; i++) {
      quadKey = quadKeys[i];

      // In order to reduce network requests, zoom out the quadkey
      // to zoom level 7 to load a larger area.
      quadKey = quadKey.substr(0, 7);
      if (!this.seenQuadKeys[quadKey]) {
        this.seenQuadKeys[quadKey] = true;
        _fetchLocations(this, quadKey);
      }
    }
  };

  Locations.prototype.updateTimeslot = function(timeslot) {
    var i, location;

    this.timeslot = timeslot;

    for (i = 0; i < this.locationMarkers.length; i++) {
      location = this.locationMarkers[i];
      location.updateTimeslot(timeslot.utc);
    }
  };

  return Locations;

});
