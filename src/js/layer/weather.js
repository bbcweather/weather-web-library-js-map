define([
], function() {

  var WeatherLayer;

  WeatherLayer = function(layer) {
    this._currentLayer = layer;
  };

  WeatherLayer.prototype.onAdd = function(map) {
    this._map = map;
    this._map.addLayer(this._currentLayer);
  };

  WeatherLayer.prototype.showLayer = function(layer) {
    if (this._currentLayer !== layer) {
      if (this._currentLayer) {
        this._map.removeLayer(this._currentLayer);
      }
      this._map.addLayer(layer);
      this._currentLayer = layer;
    }
  };

  WeatherLayer.prototype.onRemove = function(map) {
    map.removeLayer(this._currentLayer);
    this._map = null;
  };

  return WeatherLayer;

});
