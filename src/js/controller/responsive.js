define([
  'weather/map/map',
  'weather/map/layer/tilechef',
  'weather/map/layer/weather',
  'weather/map/layer/locations',
  'weather/map/control/fullscreen_button',
  'weather/map/control/close_button',
  'weather/map/control/key_button',
  'weather/map/control/key_panel',
  'weather/map/control/daytime',
  'weather/map/control/stepper',
  'weather/map/control/play_button',
  'weather/map/control/seek',
  'weather/map/control/control_group',
  'weather/map/control/filler',
  'weather/map/control/timeslots',
  'weather/map/util/dom',
  'weather/map/util/browser'
], function(Map, TilechefLayer, WeatherLayer, Locations, FullscreenButton, CloseButton, KeyButton, KeyPanel, DayTime, Stepper, PlayButton, Seek, ControlGroup, Filler, Timeslots, dom, browser) {
  'use strict';

  var Controller;

  function _initLayers(self, layers) {
    var baseLayer;

    layers = layers || {};

    baseLayer = new TilechefLayer(layers.baseTileUrl ||
        'http://{s}.bbcimg.co.uk/tilechef/{recipe}/live/base/1384949083665/{source}.png',
        {
          bounds: self.map.getMaxBounds()
        }
    );
    self.map.addLayer(baseLayer);

    self.api.map.layers().then(function(layers) {
      _initTimeslotControls(self, layers);
      _initWeatherLayer(self);
      _initLocationMarkers(self);
    });

  }

  function _initTimeslotControls(self, layers) {
    var hourlyTimeslots, dailyTimeslots;
    hourlyTimeslots = _generateTimeslots(self, layers.ukCompositeHourly);
    dailyTimeslots = _generateTimeslots(self, layers.ukCompositeDaily);
    self.timeslots = new Timeslots(hourlyTimeslots, dailyTimeslots);

    self.seek = new Seek({
      duration: self.timeslots.getDuration(),
      markers: self.timeslots.getMarkers()
    });

    self.dayTime = new DayTime(self.timeslots.getCurrentTimeslot());
    self.map.addControl(self.dayTime);

    self.playbackControlGroup = new ControlGroup({
      name: 'playback',
      controls: [
        self.seek,
        self.playButton,
        self.stepper,
        new Filler()
      ]
    });
    self.map.addControl(self.playbackControlGroup);

    self.stepper.events.next.add(function() {
      self.timeslots.next();
      self.timeslots.pause();
    });

    self.stepper.events.previous.add(function() {
      self.timeslots.previous();
      self.timeslots.pause();
    });

    self.seek.events.seeking.add(function(position) {
      self.timeslots.goToTimeslotAtPosition(position);
      self.timeslots.pause();
    });

    self.timeslots.events.timeslotChanged.add(function(timeslot) {
      self.dayTime.updateDateTime(timeslot);
      self.stepper.hasNext(!self.timeslots.isAtEnd());
      self.stepper.hasPrevious(!self.timeslots.isAtStart());
      self.weatherLayer.showLayer(timeslot.layer);
    });

    self.timeslots.events.positionChanged.add(function(position) {
      self.seek.setPosition(position);
    });

    self.timeslots.events.paused.add(function() {
      if (self.timeslots.isAtEnd()) {
        self.playButton.showReplay();
      } else {
        self.playButton.showPlay();
      }
    });

    self.playButton.events.playClick.add(function() {
      self.timeslots.play();
    });

    self.playButton.events.replayClick.add(function() {
      self.timeslots.first();
      self.timeslots.play();
    });

    self.playButton.events.pauseClick.add(function() {
      self.timeslots.pause();
    });

  }

  function _initWeatherLayer(self) {
    self.weatherLayer = new WeatherLayer(self.timeslots.getCurrentTimeslot().layer);
    self.map.addLayer(self.weatherLayer);
  }

  function _initLocationMarkers(self) {

    self.locations = new Locations({
      api: self.api,
      timeslot: self.timeslots.getCurrentTimeslot(),
      activeLocation: self.activeLocation,
      temperatureUnit: self.temperatureUnit
    });

    self.map.events.moveEnd.add(function() {
      self.locations.processBoundsChange(
        self.map.getBounds(), self.map.getZoom()
      );
    });

    self.locations.events.locationAdded.add(function(locationMarker) {
      self.map.addMarker(locationMarker);
      locationMarker.hideIfOverlappingWithLocations(self.locations.locationMarkers);
    });

    self.timeslots.events.timeslotChanged.add(function(timeslot) {
      self.locations.updateTimeslot(timeslot);
    });

    self.locations.processBoundsChange(self.map.getBounds(), self.map.getZoom());
  }

  function _generateTimeslots(self, layer) {
    var tileUrl, timeslots, timeslot;

    tileUrl = _populateTileUrlWithLayerData(layer);
    timeslots = [];
    for (var i = 0; i < layer.timeslots.length; i++) {
      timeslot = layer.timeslots[i];
      timeslots.push({
        day: timeslot.day,
        dayShort: timeslot.dayShort,
        time: timeslot.time,
        utc: timeslot.utc,
        layer: new TilechefLayer(tileUrl.replace('{utc}', timeslot.utc),
          {
            bounds: self.map.getMaxBounds()
          }
        ),
        isInitial : timeslot.isInitial,
        dateTime : timeslot.dateTime
      });
    }

    return timeslots;
  }

  function _populateTileUrlWithLayerData(data) {
    var url, template;

    template = 'http://{s}.bbcimg.co.uk/tilechef/{recipe}/live/{layer}/{version}/{sublayer}/{utc}/{source}.png';

    url = template.replace('{layer}', data.id)
                  .replace('{version}', data.version)
                  .replace('{sublayer}', data.sublayer);

    return url;
  }

  function _handleDesktopChange(self, mediaQueryList) {

    if (!self.isFullscreen) {

      if (mediaQueryList.matches) {

        if (!browser.hasTouchSupport()) {
          dom.addClass(self.container, 'weather-map--desktop');
          self.map.activate();
        }
        self.map.invalidateSize();

      }

      if (!mediaQueryList.matches || browser.hasTouchSupport()) {

        self.map.deactivate();
        self.map.reset();
        self.keyPanel.close();

        // Only skip back to the first weather layer if we've actually loaded it already
        if (self.timeslots) {
          self.timeslots.pause();
          self.timeslots.first();
        }

        dom.removeClass(self.container, 'weather-map--desktop');
        self.map.invalidateSize();

      }

    }

  }

  function _bindEvents(self) {
    self.fullscreenButton.events.click.add(function() {
      self.open();
      self.closeButton.focus();
    });

    self.closeButton.events.click.add(function() {
      self.close();
      self.fullscreenButton.focus();
    });

    self.keyButton.events.click.add(function() {
      self.keyPanel.open();
      self.keyPanel.focus();
    });

    self.keyPanel.events.close.add(function() {
      self.keyButton.focus();
    });

    if (self.mode === Controller.modes.auto) {

      if (window.matchMedia) {

        self.desktopMediaQuery = window.matchMedia('screen and (min-width: 1008px)');
        self.desktopMediaQuery.addListener(function(mediaQueryList) {
          _handleDesktopChange(self, mediaQueryList);
        });
        _handleDesktopChange(self, self.desktopMediaQuery);

      } else {

        // If matchMedia is not supported we are likely on an IE8 browser
        // so always show the embedded desktop mode.
        dom.addClass(self.container, 'weather-map--desktop');
        self.map.activate();
        self.map.invalidateSize();

      }

    } else {
      // Teaser mode so deactivate the map
      self.map.deactivate();
    }

    if (typeof window.onhashchange !== 'undefined') {
      window.addEventListener('hashchange', function() {
        if (window.location.hash === '#map-fullscreen' && !self.isFullscreen) {
          self.open();
        } else if (window.location.hash === '' && self.isFullscreen) {
          self.close();
        }
      });
    }
  }

  Controller = function(options) {

    options = options || {};

    this.api = options.api;
    this.container = options.container;
    this.isFullscreen = false;
    this.temperatureUnit = options.temperatureUnit || 'c';
    this.activeLocation = options.activeLocation;
    this.desktopMediaQuery = {};
    this.mode = options.mode || Controller.modes.auto;

    this.map = new Map({
      container: this.container,
      center: options.center,
      zoom: options.zoom
    });

    this.fullscreenButton = new FullscreenButton();
    this.closeButton = new CloseButton();
    this.keyButton = new KeyButton();
    this.keyPanel = new KeyPanel();
    this.stepper = new Stepper();
    this.playButton = new PlayButton();

    this.map.addControl(this.closeButton);
    this.map.addControl(this.fullscreenButton);
    this.map.addControl(this.keyButton);
    this.map.addControl(this.keyPanel);

    if (!browser.hasTouchSupport()) {
      dom.addClass(this.container, 'weather-map--no-touch');
    }

    _initLayers(this, options.layers);
    _bindEvents(this);
  };

  Controller.prototype.open = function() {
    this.map.activate();
    dom.addClass(this.container, 'weather-map--fullscreen');
    this.isFullscreen = true;
    window.location.hash = 'map-fullscreen';
    this.map.invalidateSize();
  };

  Controller.prototype.close = function() {
    dom.removeClass(this.container, 'weather-map--fullscreen');
    this.isFullscreen = false;
    _handleDesktopChange(this, this.desktopMediaQuery);
    this.map.reset();
    window.location.hash = '';
    this.map.invalidateSize();
  };

  Controller.modes = {
    auto: 1,
    teaser: 2
  };

  return Controller;

});
