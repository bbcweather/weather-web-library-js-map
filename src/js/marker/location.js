define([
  'vendor/leaflet/leaflet',
  'weather/map/util/dom'
], function(leaflet, dom) {
  'use strict';

  var Location;

  Location = function(data, options) {
    options = options || {};
    this.temperatureUnit = options.temperatureUnit || 'c';
    this.timeslot = options.timeslot;
    this.data = data;
    this.latlng = leaflet.latLng(data.coordinates[1], data.coordinates[0]);
  };

  Location.prototype.onAdd = function(map) {

    var element,
        locationElement,
        temperatureElement,
        temperature;

    this.map = map;

    this.uniqueClassName = ('location-marker__temperature-' + this.data.id);

    element = dom.create('div', 'location-marker');
    element.setAttribute('aria-hidden', 'true');

    dom.create('div', 'location-marker__point', element);

    locationElement = dom.create('div', 'location-marker__location', element);
    locationElement.innerHTML = this.data.name;

    temperatureElement = dom.create('div', 'location-marker__temperature ' + this.uniqueClassName, element);
    temperature = this.data.forecasts[this.timeslot].temperature;
    _updateTemperature(temperatureElement, temperature, this.temperatureUnit);

    return leaflet.divIcon({
      className: 'weather-map__location-marker',
      html: element.outerHTML,
      iconAnchor: leaflet.point(0, 0)
    });
  };

  Location.prototype.updateTimeslot = function(timeslot) {
    var temperature = this.data.forecasts[timeslot].temperature;
    _updateTemperature(getTemperatureElement(this), temperature, this.temperatureUnit);
  };

  Location.prototype.hideIfOverlappingWithLocations = function(locations) {
    this.offsets = getTransform(getTemperatureElement(this));
    for (var i = 0; i < locations.length; i++) {
      var otherLocation = locations[i];

      if (this.data.id !== otherLocation.data.id &&
      !otherLocation.isHidden() &&
      this.offsets.left <= otherLocation.offsets.right &&
      this.offsets.right >= otherLocation.offsets.left &&
      this.offsets.top <= otherLocation.offsets.bottom &&
      this.offsets.bottom >= otherLocation.offsets.top) {
        if (this.data.type === 'ACTIVE') {
          otherLocation.hide();
        } else {
          this.hide();
        }
      }
    }
  };

  Location.prototype.hide = function() {
    this.hidden = true;
    dom.addClass(getMarkerElement(this), 'weather-map__location-marker--hidden');
  };

  Location.prototype.isHidden = function() {
    return this.hidden;
  };

  function getMarkerElement(self) {
    if (!self.markerElement) {
      self.markerElement = getTemperatureElement(self).parentNode.parentNode;
    }
    return self.markerElement;
  }

  function getTemperatureElement(self) {
    if (!self.temperatureElement) {
      self.temperatureElement = self.map.querySelector('.' + self.uniqueClassName);
    }
    return self.temperatureElement;
  }

  function getTransform(temperatureElement) {
    // Leaflet uses transform (where possible) or top/left to position the label, so use those to work out the positions on the map
    var element, computedStyle, transformValue, matrixValues, transformXY, transform;
    element = temperatureElement.parentNode.parentNode;
    computedStyle = element.currentStyle || window.getComputedStyle(element, null); // currentStyle is IE, getComputedStyle is other browsers
    transformValue = computedStyle.transform || computedStyle['-webkit-transform'] || computedStyle['-ms-transform'];
    if (transformValue && transformValue !== 'none') {
      // If leaflet used transform (most browsers), use these values
      matrixValues = transformValue.match(/matrix\(([^\)]+)\)/)[1];
      transformXY = matrixValues.split(', ').slice(4);
    } else {
      // otherwise it used left/top (in IE), so use these values
      transformXY = [computedStyle.left, computedStyle.top];
    }

    transform = {
      left: parseFloat(transformXY[0]) - temperatureElement.offsetWidth,
      right: parseFloat(transformXY[0]) + element.offsetWidth,
      top: parseFloat(transformXY[1]),
      bottom: parseFloat(transformXY[1]) + element.offsetHeight
    };

    return transform;
  }

  function _updateTemperature(element, temperature, temperatureUnit) {
    if (temperature && typeof temperature[temperatureUnit] !== 'undefined' && temperature[temperatureUnit] !== null) {
      element.innerHTML = temperature[temperatureUnit];
    } else {
      element.innerHTML = '-';
    }

    var temperatureClass = 'temperature-na';
    if (temperature && temperature.c != null) {
      if (temperature.c >= 25) {
        temperatureClass = 'temperature-25-up';
      } else if (temperature.c <= 0) {
        temperatureClass = 'temperature-0-down';
      } else {
        for (var t = 22; t > 0; t = t - 3) {
          if (temperature.c >= t) {
            temperatureClass = 'temperature-' + t + '-' + ( t + 2 );
            break;
          }
        }
      }
    }

    dom.removeClass(element, 'temperature-.*');
    dom.addClass(element, temperatureClass);
  }

  return Location;

});
