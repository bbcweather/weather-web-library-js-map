weather-web-library-js-map
==========================

Map javascript library for displaying weather maps, built upon the [Leaflet][]
map library.

[Leaflet]: https://github.com/gruntjs/grunt/blob/devel/docs/getting_started.md


Using the library
-----------------

To use the library in your project you can either download a release and
manually copy the distribution files into your project, or use a dependency
manager such as [bower][].


### Bower Install

To use bower, first make sure it is installed:

```bash
npm install -g bower
```

Then create a bower.json file in the root of your project:
```json
{
  "name": "my-amazing-project",
  "version": "0.0.1",
  "dependencies": {
    "weather-web-library-js-map": "https://bitbucket.org/bbcweather/weather-web-library-js-map.git#v0.0.1"
  }
}
```

And run bower install:

```bash
bower install
```

By default bower will install to directory named 'bower_components', you can
either configure it to install to your desired location or implement a script
or task to copy the necessary files out of this directory.

[bower]: http://bower.io/


### Require JS Configuration

Below is a sample page which shows require js configuration and instantiation
of a map.

```html
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="/js/vendor/weather-map/map.css">
    <script src="/js/vendor/requirejs/require.js"></script>
    <script>
        require.config({
          baseUrl: '/js',
          path: {
            map: '/js/vendor/weather-map/map'
        });
    </script>
</head>
<body>
<div id="map"></div>
<script>
  require('map', function(Map) {
    var map = new Map({
      elementId: 'map'
    });
  });
</script>
</body>
</html>
```


Getting Started
---------------

The build system uses [grunt][], be sure to check the [Getting Started][] guide
if you haven't used it before.  It is usually best to install grunt globally,
unless you don't mind running it from the node_modules/bin folder:

```bash
npm install grunt-cli -g
```

From the root directory of the project, install node dependencies with:

```bash
npm install
```

And setup all the libraries with:

```bash
grunt init
```

Once installed the unit test suite can be run with:

```bash
grunt test
```

To preview a map in the browser run:

```bash
grunt serve
```

You can then visit http://localhost:9090/ to view the map.

[grunt]: http://gruntjs.com/
[Getting Started]: https://github.com/gruntjs/grunt/blob/devel/docs/getting_started.md



Release Instructions
--------------------

To release a new version of the library, do the following:

* Bump the version number in [package.json][] using [semantic versioning][]
* Commit the version bump to master and push the change
* Run the publish script: ./tasks/publish.sh

The publish script performs the following actions:

* Picks up the version number from package json
* Checks out a 'build' branch, runs tests and the 'prepublish' script to
  generates the distribution files
* Bumps the version number in bower json using [tin][]
* Commits, tags and then pushes the tag

[package.json]: https://docs.npmjs.com/files/package.json
[semantic versioning]: http://semver.org/
[tin]: https://www.npmjs.com/package/tin
