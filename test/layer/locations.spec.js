define([
  'weather/api',
  'weather/map/marker/location',
  'weather/map/layer/locations',
  'signals',
  'vendor/leaflet/leaflet'
], function(Api, Location, Locations, signals, leaflet) {
  'use strict';

  describe('layers.locations', function() {

    var api,
        timeslot,
        temperatureUnit,
        locationsLayer;

    beforeEach(function() {
      api = new Api({
        environment: 'live',
        locale: 'en',
        timezone: 'Europe/London'
      });

      timeslot = { utc: '15040200' };
      temperatureUnit = 'f';

      locationsLayer = new Locations({
        api: api,
        temperatureUnit: temperatureUnit,
        timeslot: timeslot,
        activeLocation: '6691484'
      });
    });

    describe('constructor', function() {

      it('sets the active location to the options', function() {
        expect(locationsLayer.activeLocation).toEqual('6691484');
      });

      it('sets the API to the options', function() {
        expect(locationsLayer.api).toEqual(api);
      });

      it('sets the timeslot to the options', function() {
        expect(locationsLayer.timeslot).toBe(timeslot);
      });

      it('sets the temperatureUnit to the options', function() {
        expect(locationsLayer.temperatureUnit).toBe(temperatureUnit);
      });

      it('initialises a list of seen quad keys', function() {
        expect(locationsLayer.seenQuadKeys).toEqual([]);
      });

      it('initialises a list of locations', function() {
        expect(locationsLayer.locationMarkers).toEqual([]);
      });

      it('sets up the location added event', function() {
        expect(locationsLayer.events.locationAdded instanceof signals.Signal).toBe(true);
      });

      it('calls the API to get the active location', function() {
        spyOn(api.map, 'locationsFromLocationId').and.returnValue(new Promise(function() {}));

        locationsLayer = new Locations({
          api: api,
          temperatureUnit: temperatureUnit,
          timeslot: timeslot,
          activeLocation: '6691484'
        });

        expect(api.map.locationsFromLocationId).toHaveBeenCalledWith('6691484');
      });

    });

    describe('#processBoundsChange', function() {

      it('calls the API to get the locations in the visible tiles', function() {
        var bounds, zoom;
        bounds = leaflet.latLngBounds(
          leaflet.latLng(61.603, -10.854),
          leaflet.latLng(49.022, 2.7125)
        );
        zoom = 9;

        spyOn(api.map, 'locationsFromQuadKey').and.returnValue(new Promise(function() {}));

        locationsLayer.processBoundsChange(bounds, zoom);

        expect(api.map.locationsFromQuadKey).toHaveBeenCalled();
      });

    });

    describe('#updateTimeslot', function() {

      it('updates the timeslot on each location', function() {
        var data, location1, location2;
        data = {
          coordinates: [50, 0]
        };
        location1 = new Location(data);
        location2 = new Location(data);
        locationsLayer.locationMarkers = [location1, location2];

        spyOn(location1, 'updateTimeslot');
        spyOn(location2, 'updateTimeslot');

        locationsLayer.updateTimeslot(timeslot);

        expect(location1.updateTimeslot).toHaveBeenCalledWith(timeslot.utc);
        expect(location2.updateTimeslot).toHaveBeenCalledWith(timeslot.utc);
      });

    });

  });

});
