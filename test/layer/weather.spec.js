define([
  'weather/map/map',
  'weather/map/layer/weather',
  'weather/map/layer/tilechef'
], function(Map, WeatherLayer, Tilechef) {
  'use strict';

  describe('layer.weather', function() {

    var map,
      container,
      weather,
      layer,
      layer2;

    beforeEach(function() {
      layer = new Tilechef('http://example.com');
      layer2 = new Tilechef('http://anotherexample.com');
      container = document.createElement('div');
      document.body.appendChild(container);
      map = new Map({
        container: container
      });
      weather = new WeatherLayer(layer);
      weather.onAdd(map);
    });

    afterEach(function() {
      document.body.removeChild(container);
    });

    describe('#onAdd', function() {

      it('adds the inital layer to the map', function() {
        expect(map.engine.hasLayer(layer)).toBe(true);
      });

    });

    describe('#showLayer', function() {

      it('adds the layer to the map', function() {
        weather.showLayer(layer2);
        expect(map.engine.hasLayer(layer2)).toBe(true);
      });

      it('removes the old layer from the map', function() {
        weather.showLayer(layer2);
        expect(map.engine.hasLayer(layer)).toBe(false);
      });

    });

    describe('#onRemove', function() {

      it('removes the current layer from the map', function() {
        weather.showLayer(layer2);
        weather.onRemove(map);
        expect(map.engine.hasLayer(layer2)).toBe(false);
      });

    });

  });

});
