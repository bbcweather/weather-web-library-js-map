define([
  'weather/map/layer/tilechef'
], function(Tilechef) {

  describe('layer.tilechef', function() {

    var layer,
        url;

    beforeEach(function() {
      url = 'http://{s}.example.com/tiles/{recipe}/{source}';
      layer = new Tilechef(url);
    });

    describe('#initialize', function() {

      it('sets the url property', function() {
        expect(layer._url).toEqual(url);
      });

      it('defaults the subdomain to array of available tile servers', function() {
        expect(layer.options.subdomains).toEqual(['tile1', 'tile2', 'tile3', 'tile4']);
      });

      it('defaults the minZoom level to 7', function() {
        expect(layer.options.minZoom).toEqual(7);
      });

      it('defaults the maxNativeZoom level to 10', function() {
        expect(layer.options.maxNativeZoom).toEqual(10);
      });

    });

    describe('#getTileUrl', function() {

      var coords = { x: 0, y: 0, zoom: 2 };

      it('replaces {s} in the base url with a subdomain', function() {
        spyOn(layer, '_getSubdomain').and.returnValue('subdomain');
        spyOn(layer, '_getZoomForUrl').and.returnValue(coords.zoom);

        expect(layer.getTileUrl(coords)).toEqual(
          'http://subdomain.example.com/tiles/-/00'
        );
      });

      /*
       * The source tiles are oversized (2048px) but named with the quad key area
       * they represent. Tilechef recipes are used to crop and rescale the sources
       * tiles into 256px tiles to fill in missing zoom levels, the source tiles
       * are (where 0 can be any number from 0-3):
       *  0.png         - Zoom level 4 (World)
       *  00.png        - Zoom level 5 (World)
       *  0000.png      - Zoom level 7 (Europe)
       *  0000000.png   - Zoom level 10 (UK)
       *
       * The recipes add up to 3 additional zoom levels:
       *  -     - Resizes 12.5% (source tile is original tile resized)
       *  0     - Resizes 25% (512px), crops to 256px (source tile split into 4 tiles)
       *  00    - Resizes 50% (1024px), crops to 256px (source tile split into 16 tiles)
       *  000   - Crops to 256px (source tile split into 64 tiles)
       *
       * The recipe is formed from the last digits of the quad key.  At zoom level
       * 8 the quad key is 8 digits long (the length of the quad key matches the
       * zoom level) the source tile is only 7 digits long (but equivalent to zoom
       * level 10) so the quad key is split after 7 digits and the last digit becomes
       * the recipe to scale the image to zoom level 8 and crop to the correct subtile.
       */

      it('replaces {recipe} in the url with "-" for zoom 7', function() {
        coords.zoom = 7;

        spyOn(layer, '_getSubdomain').and.returnValue('subdomain');
        spyOn(layer, '_getZoomForUrl').and.returnValue(coords.zoom);

        expect(layer.getTileUrl(coords)).toEqual(
          'http://subdomain.example.com/tiles/-/0000000'
        );
      });

      it('replaces {recipe} in the url with a one digit Tilechef recipe for zoom 8', function() {
        coords.zoom = 8;

        spyOn(layer, '_getSubdomain').and.returnValue('subdomain');
        spyOn(layer, '_getZoomForUrl').and.returnValue(coords.zoom);

        expect(layer.getTileUrl(coords)).toEqual(
          'http://subdomain.example.com/tiles/0/0000000'
        );
      });

      it('replaces {recipe} in the url with a two digit Tilechef recipe for zoom 9', function() {
        coords.zoom = 9;

        spyOn(layer, '_getSubdomain').and.returnValue('subdomain');
        spyOn(layer, '_getZoomForUrl').and.returnValue(coords.zoom);

        expect(layer.getTileUrl(coords)).toEqual(
          'http://subdomain.example.com/tiles/00/0000000'
        );
      });

      it('replaces {recipe} in the url with a two digit Tilechef recipe for zoom 10', function() {
        coords.zoom = 10;

        spyOn(layer, '_getSubdomain').and.returnValue('subdomain');
        spyOn(layer, '_getZoomForUrl').and.returnValue(coords.zoom);

        expect(layer.getTileUrl(coords)).toEqual(
          'http://subdomain.example.com/tiles/000/0000000'
        );
      });

      /*
       * Source is the first 7 digits of the quad key, to test this change the coords
       * so the last to digits of the quad key change.
       */
      it('replaces {sources} with the first 7 digits of the quad key', function() {
        // geo.quadKeyFromTileXYZ(3, 0, 9) returns 000000011;
        coords.x = 3;
        coords.y = 0;
        coords.zoom = 9;

        spyOn(layer, '_getSubdomain').and.returnValue('subdomain');
        spyOn(layer, '_getZoomForUrl').and.returnValue(coords.zoom);

        expect(layer.getTileUrl(coords)).toEqual(
          'http://subdomain.example.com/tiles/11/0000000'
        );
      });

    });

  });

});
