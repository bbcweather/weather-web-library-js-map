define([
  'weather/map/map',
  'weather/map/control/control_group',
  'weather/map/control/control',
  'weather/map/control/filler',
  'weather/map/util/dom'
], function(Map, ControlGroup, Control, Filler, dom) {
  'use strict';

  describe('control.control_group', function() {

    var controlGroup,
        testControl;

    beforeEach(function() {
      testControl = new Filler();
      controlGroup = new ControlGroup({
        name: 'test',
        controls: [ testControl ]
      });
    });

    describe('constructor', function() {

      it ('is an instance of Control', function() {
        expect(controlGroup instanceof Control).toBe(true);
      });

      it('sets the name property from options', function() {
        expect(controlGroup.name).toEqual('test');
      });

      it('sets the controls from options', function() {
        expect(controlGroup.controls).toEqual([ testControl ]);
      });

    });

    describe('#onAdd', function() {

      var element,
          map;

      beforeEach(function() {
        map = 'instance-of-a-map';
        spyOn(testControl, 'onAdd').and.callThrough();
        element = controlGroup.onAdd(map);
        document.body.appendChild(element);
      });

      afterEach(function() {
        document.body.removeChild(element);
      });

      it('returns an element with a control-group class name', function() {
        expect(dom.hasClass(element, 'control-group')).toBe(true);
      });

      it('uses name as a modifier class', function() {
        var className = 'control-group--' + controlGroup.name;
        expect(dom.hasClass(element, className)).toBe(true);
      });

      it('adds the controls and passes map instance to onAdd method of control', function() {
        expect(testControl.onAdd).toHaveBeenCalled();
      });

    });

  });

});
