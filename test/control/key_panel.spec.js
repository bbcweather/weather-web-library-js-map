define([
  'weather/map/map',
  'weather/map/control/key_panel',
  'weather/map/control/control',
  'weather/map/util/dom',
  'test/helper/event',
  'signals',
  'vendor/jasmine-signals/jasmine-signals'
], function(Map, KeyPanel, Control, dom, event, signals, spyOnSignal) {
  'use strict';

  describe('control.key_panel', function() {

    var keyPanel;

    beforeEach(function() {
      keyPanel = new KeyPanel();
    });

    describe('constructor', function() {

      it('is an instance of Control', function() {
        expect(keyPanel instanceof Control).toBe(true);
      });

      it('sets up the events', function() {
        expect(keyPanel.events).toBeDefined();
      });

      it('sets up a close event', function() {
        expect(keyPanel.events.close instanceof signals.Signal).toBe(true);
      });

      it('sets up an open event', function() {
        expect(keyPanel.events.open instanceof signals.Signal).toBe(true);
      });

    });

    describe('#onAdd', function() {

      it('returns an Element', function() {
        expect(keyPanel.onAdd() instanceof Element).toBe(true);
      });

      it('returns an element with a key panel class', function() {
        var element = keyPanel.onAdd();

        expect(dom.hasClass(element, 'key-panel')).toBe(true);
      });

      it('returns an element with the right title', function() {
        var title = keyPanel.onAdd().querySelector('.key-panel__title');

        expect(title.innerHTML).toBe('Key');
      });

      it('returns an element with five key items', function() {
        var items = keyPanel.onAdd().querySelectorAll('.key-panel__row');

        expect(items.length).toEqual(5);
      });

      it('returns an element with each key item containing a label', function() {
        var items = keyPanel.onAdd().querySelectorAll('.key-panel__row');

        for (var i = 0; i < items.length; i++) {
          var labels = items[i].querySelectorAll('.key-panel__label');
          expect(labels.length).toEqual(1);
        }
      });

      it('returns an element with each key item containing two indicators', function() {
        var items = keyPanel.onAdd().querySelectorAll('.key-panel__row');

        for (var i = 0; i < items.length; i++) {
          var indicators = items[i].querySelectorAll('.key-panel__indicator');
          expect(indicators.length).toEqual(2);
        }
      });

      it('returns an element with each key item containing one or two gradients', function() {
        var items = keyPanel.onAdd().querySelectorAll('.key-panel__row');

        for (var i = 0; i < items.length; i++) {
          var gradients = items[i].querySelectorAll('.key-panel__gradient');
          expect(gradients.length).toBeGreaterThan(0);
          expect(gradients.length).toBeLessThan(3);
        }
      });

      it('close button closes the key when clicked', function() {
        var closeButtonEl = keyPanel.onAdd().querySelector('.key-panel__close-button');

        spyOn(keyPanel, 'close');

        event.trigger(closeButtonEl, 'click');

        expect(keyPanel.close).toHaveBeenCalled();
      });
    });

    describe('#open', function() {

      it('sets the open class on the key panel', function() {
        var element;

        element = keyPanel.onAdd();
        keyPanel.open();
        keyPanel.close();
        keyPanel.open();

        expect(dom.hasClass(element, 'key-panel--open')).toBe(true);
      });

      it('fires an open event', function() {
        spyOnSignal(keyPanel.events.open);

        keyPanel.onAdd();
        keyPanel.open();

        expect(keyPanel.events.open).toHaveBeenDispatched();
      });

    });

    describe('#close', function() {

      it('removes the open class on the key panel', function() {
        var element;

        element = keyPanel.onAdd();
        keyPanel.open();
        keyPanel.close();

        expect(dom.hasClass(element, 'key-panel--open')).toBe(false);
      });

      it('fires a close event', function() {
        spyOnSignal(keyPanel.events.close);

        keyPanel.onAdd();
        keyPanel.open();
        keyPanel.close();

        expect(keyPanel.events.close).toHaveBeenDispatched();
      });

      it('does not fire a close event if the panel is not open', function() {
        spyOnSignal(keyPanel.events.close);

        keyPanel.onAdd();
        keyPanel.close();

        expect(keyPanel.events.close).not.toHaveBeenDispatched();
      });

    });

  });

});
