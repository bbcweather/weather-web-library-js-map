define([
  'weather/map/map',
  'weather/map/control/timeslots',
  'weather/map/layer/tilechef',
  'vendor/jasmine-signals/jasmine-signals',
  'signals'
], function(Map, Timeslots, Tilechef, spyOnSignal, signals) {
  'use strict';

  describe('control.timeslots', function() {

    var map,
        container,
        timeslots,
        hourlyTimeslots,
        dailyTimeslots;

    beforeEach(function() {
      container = document.createElement('div');
      document.body.appendChild(container);
      map = new Map({
        container: container
      }); 
      hourlyTimeslots = [
        {
          dateTime: 1430308800000,
          dayShort: 'Thu'
        },
        {
          dateTime: 1430319600000,
          dayShort: 'Thu',
          isInitial: '1'
        },
        {
          dateTime: 1430330400000,
          dayShort: 'Thu'
        },
        {
          dateTime: 1430341200000,
          dayShort: 'Thu'
        }
      ];
      dailyTimeslots = [
        {
          dateTime: 1430352000000,
          dayShort: 'Fri'
        },
        {
          dateTime: 1430395200000,
          dayShort: 'Fri',
          isInitial: '1'
        },
        {
          dateTime: 1430438400000,
          dayShort: 'Sat'
        },
        {
          dateTime: 1430481600000,
          dayShort: 'Sat'
        }
      ];
      timeslots = new Timeslots(hourlyTimeslots, dailyTimeslots);
    });

    afterEach(function() {
      document.body.removeChild(container);
    });

    describe('constructor', function() {

      it('sets up the events', function() {
        expect(timeslots.events).toBeDefined();
      });

      it('sets up a timeslotChanged event', function() {
        expect(timeslots.events.timeslotChanged instanceof signals.Signal).toBe(true);
      });

      it('sets up a positionChanged event', function() {
        expect(timeslots.events.positionChanged instanceof signals.Signal).toBe(true);
      });

      it('sets up a paused event', function() {
        expect(timeslots.events.paused instanceof signals.Signal).toBe(true);
      });

    });

    describe('#first', function() {

      it('sets the current timeslot to the initial timeslot', function() {
        timeslots.first();

        expect(timeslots.getCurrentTimeslot()).toEqual(hourlyTimeslots[1]);
      });

      it('emits the timeslotChanged signal', function() {
        spyOnSignal(timeslots.events.timeslotChanged);

        timeslots.first();

        expect(timeslots.events.timeslotChanged).toHaveBeenDispatched();
      });

      it('emits the positionChanged event', function() {
        spyOnSignal(timeslots.events.positionChanged);

        timeslots.first();

        expect(timeslots.events.positionChanged).toHaveBeenDispatched();
      });

    });

    describe('#next', function() {

      it('advances the timeslot', function() {
        expect(timeslots.getCurrentTimeslot()).toEqual(hourlyTimeslots[1]);

        timeslots.next();

        expect(timeslots.getCurrentTimeslot()).toEqual(hourlyTimeslots[2]);
      });

      it('does not advance the index beyond the number of timeslots', function() {
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();

        expect(timeslots.getCurrentTimeslot()).toEqual(dailyTimeslots[3]);
      });

      it('emits the timeslot changed signal', function() {
        spyOnSignal(timeslots.events.timeslotChanged);

        timeslots.next();

        expect(timeslots.events.timeslotChanged).toHaveBeenDispatched();
      });

      it('emits the position changed signal', function() {
        spyOnSignal(timeslots.events.positionChanged);

        timeslots.next();

        expect(timeslots.events.positionChanged).toHaveBeenDispatched();
      });

    });

    describe('#previous', function() {

      it('decreases the timeslot index', function() {
        timeslots.next();
        timeslots.next();
        timeslots.previous();

        expect(timeslots.getCurrentTimeslot()).toEqual(hourlyTimeslots[2]);
      });

      it('does not decrease the index below the index of the initial timeslot', function() {
        timeslots.previous();

        expect(timeslots.getCurrentTimeslot()).toEqual(hourlyTimeslots[1]);
      });

      it('emits the timeslot changed signal', function() {
        timeslots.next();
        spyOnSignal(timeslots.events.timeslotChanged);

        timeslots.previous();

        expect(timeslots.events.timeslotChanged).toHaveBeenDispatched();
      });

      it('emits the position changed signal', function() {
        timeslots.next();
        spyOnSignal(timeslots.events.positionChanged);

        timeslots.previous();

        expect(timeslots.events.positionChanged).toHaveBeenDispatched();
      });

    });

    describe('#isAtStart', function() {

      it('returns true if the index is at zero', function() {
        expect(timeslots.isAtStart()).toBe(true);
      });

      it('returns false if the index is not at zero', function() {
        timeslots.next();

        expect(timeslots.isAtStart()).toBe(false);
      });

    });

    describe('#isAtEnd', function() {

      it('returns true if the index equals the number of timeslots', function() {
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        timeslots.next();
        expect(timeslots.isAtEnd()).toBe(true);
      });

      it('returns false if the index does not equal the number of timeslots', function() {
        expect(timeslots.isAtEnd()).toBe(false);
      });

    });

    describe('#getCurrentTimeslot', function() {

      it('returns the current timeslot', function() {
        expect(timeslots.getCurrentTimeslot()).toEqual(hourlyTimeslots[1]);

        timeslots.next();
        expect(timeslots.getCurrentTimeslot()).toEqual(hourlyTimeslots[2]);
      });

    });

    describe('#goToTimeslotAtPosition', function() {

      it('updates the current timeslot to the one at the valid requested position in hourly timeslots', function() {
        timeslots.goToTimeslotAtPosition(10800000);
        expect(timeslots.getCurrentTimeslot()).toEqual(hourlyTimeslots[2]);
      });

      it('updates the current timeslot to the one at the valid requested position in daily timeslots', function() {
        timeslots.goToTimeslotAtPosition(118800000);
        expect(timeslots.getCurrentTimeslot()).toEqual(dailyTimeslots[2]);
      });

      it('emits the timeslotChanged signal if we are changing to a different timeslot', function() {
        timeslots.goToTimeslotAtPosition(10800000);
        spyOnSignal(timeslots.events.timeslotChanged);
        timeslots.goToTimeslotAtPosition(118800000);
        expect(timeslots.events.timeslotChanged).toHaveBeenDispatched();
      });

      it('does not emit the timeslotChanged signal if we try to change to a position covering the same timeslot', function() {
        timeslots.goToTimeslotAtPosition(10800000);
        spyOnSignal(timeslots.events.timeslotChanged);
        timeslots.goToTimeslotAtPosition(10800001);
        expect(timeslots.events.timeslotChanged).not.toHaveBeenDispatched();
      });

    });

    describe('#getMarkers', function() {

      it('returns the set of markers for the timeslots given', function() {
        var expectedMarkers = [
          { position: 0,  label: 'Thu' },
          { position: 10800000 },
          { position: 21600000 },
          { position: 32400000, label: 'Fri' },
          { position: 75600000 },
          { position: 118800000, label: 'Sat' },
          { position: 162000000 }
        ];
        expect(timeslots.getMarkers()).toEqual(expectedMarkers);
      });

    });

    describe('#getDuration', function() {

      it('returns the difference between the last and initial timeslot times', function() {
        expect(timeslots.getDuration()).toEqual(162000000);
      });

    });

    describe('#play', function() {

      it('sets isPlaying to true', function() {
        timeslots.play();

        expect(timeslots.isPlaying()).toBe(true);
      });

      it('emits a positionChanged event', function() {
        spyOnSignal(timeslots.events.positionChanged);

        timeslots.play();

        expect(timeslots.events.positionChanged).toHaveBeenDispatched();
      });

    });

    describe('#pause', function() {

      it('sets isPlaying to false', function() {
        timeslots.play();
        timeslots.pause();

        expect(timeslots.isPlaying()).toBe(false);
      });

      it('emits a paused event', function() {
        spyOnSignal(timeslots.events.paused);

        timeslots.pause();

        expect(timeslots.events.paused).toHaveBeenDispatched();
      });

    });

    describe('#isPlaying', function() {

      it('returns false when the animation is not playing', function() {
        expect(timeslots.isPlaying()).toBe(false);
      });

      it('returns true when the animation is playing', function() {
        timeslots.play();
        expect(timeslots.isPlaying()).toBe(true);
      });

    });

  });

});
