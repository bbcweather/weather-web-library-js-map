define([
  'weather/map/map',
  'weather/map/control/close_button',
  'weather/map/control/control',
  'weather/map/util/dom',
  'test/helper/event',
  'vendor/jasmine-signals/jasmine-signals'
], function(Map, CloseButton, Control, dom, event, spyOnSignal) {
  'use strict';

  describe('control.close_button', function() {

    var closeButton,
        element;

    beforeEach(function() {
      closeButton = new CloseButton();
      element = closeButton.onAdd();
      document.body.appendChild(element);
    });

    afterEach(function() {
      document.body.removeChild(element);
    });

    describe('constructor', function() {

      it('is an instance of Control', function() {
        expect(closeButton instanceof Control).toBe(true);
      });

    });

    describe('#onAdd', function() {

      it('returns an Element', function() {
        expect(closeButton.onAdd() instanceof Element).toBe(true);
      });

      it('returns a button element', function() {
        expect(element.nodeName.toLowerCase()).toEqual('button');
      });

      it('returns an element with a "close-button" class', function() {
        expect(dom.hasClass(element, 'close-button')).toBe(true);
      });

      it('returns an element with "Close map" text', function() {
        expect(element.textContent).toEqual('Close map');
      });

      it('dispatches the click event when clicked', function() {
        spyOnSignal(closeButton.events.click);

        event.trigger(element, 'click');
        expect(closeButton.events.click).toHaveBeenDispatched();
      });

    });

    describe('#focus', function() {

      it('gives the element focus', function() {
        document.body.focus();
        closeButton.focus();

        expect(document.activeElement).toEqual(element);
      });

    });

  });

});
