define([
  'weather/map/control/daytime',
  'weather/map/control/control',
  'weather/map/util/dom'
], function(DayTime, Control, dom) {
  'use strict';

  describe('control.daytime', function() {

    var dayTime,
        dayTimeElement;

    beforeEach(function() {
      dayTime = new DayTime();
      dayTimeElement = dayTime.onAdd();
    });

    describe('constructor', function() {

      it('is an instance of Control', function() {
        expect(dayTime instanceof Control).toBe(true);
      });

    });

    describe('#onAdd', function() {

      it('returns an element', function() {
        expect(dayTimeElement instanceof Element).toBe(true);
      });

      it('returns an element with the day-time class', function() {
        expect(dom.hasClass(dayTimeElement, 'day-time')).toBe(true);
      });

      it('returns an element with two labels', function() {
        var dayTimeLabels = dayTimeElement.querySelectorAll('.day-time__label');

        expect(dayTimeLabels.length).toEqual(2);
      });

      it('returns a default element with one day label', function() {
        var dayLabels = dayTimeElement.querySelectorAll('.day-time__label--day');

        expect(dayLabels.length).toEqual(1);
      });

    });

    describe('#updateTime', function() {

      beforeEach(function() {
      });

      it('sets a normal label correctly', function() {
        dayTime.updateDateTime({
          day: 'Thursday',
          time: '09:00'
        });
        expect(dayTime.dayLabel.innerHTML).toEqual('Thursday');
        expect(dayTime.timeLabel.innerHTML).toEqual('09:00');
        expect(dom.hasClass(dayTime.dayLabel, 'day-time__label--hidden')).toBe(false);
        expect(dom.hasClass(dayTime.timeLabel, 'day-time__label--hidden')).toBe(false);
      });

      it('sets a day-time day/night label correctly', function() {
        dayTime.updateDateTime({
          day: 'Sunday',
          time: 'day'
        });
        expect(dayTime.dayLabel.innerHTML).toEqual('Sunday');
        expect(dayTime.timeLabel.innerHTML).toEqual('');
        expect(dom.hasClass(dayTime.dayLabel, 'day-time__label--hidden')).toBe(false);
        expect(dom.hasClass(dayTime.timeLabel, 'day-time__label--hidden')).toBe(true);
      });

      it('sets a night-time day/night label correctly', function() {
        dayTime.updateDateTime({
          day: 'Saturday',
          time: 'night'
        });
        expect(dayTime.dayLabel.innerHTML).toEqual('Saturday');
        expect(dayTime.timeLabel.innerHTML).toEqual('night');
        expect(dom.hasClass(dayTime.dayLabel, 'day-time__label--hidden')).toBe(false);
        expect(dom.hasClass(dayTime.timeLabel, 'day-time__label--hidden')).toBe(false);
      });

    });

  });

});
