define([
  'weather/map/map',
  'weather/map/control/fullscreen_button',
  'weather/map/control/control',
  'test/helper/event',
  'vendor/jasmine-signals/jasmine-signals'
], function(Map, FullscreenButton, Control, event, spyOnSignal) {
  'use strict';

  describe('control.fullscreen_button', function() {

    var fullscreenButton,
        element;

    beforeEach(function() {
      fullscreenButton = new FullscreenButton();
      element = fullscreenButton.onAdd();
      document.body.appendChild(element);
    });

    afterEach(function() {
      document.body.removeChild(element);
    });

    describe('constructor', function() {

      it('is an instance of Control', function() {
        expect(fullscreenButton instanceof Control).toBe(true);
      });

    });

    describe('#onAdd', function() {

      it('returns an element', function() {
        expect(fullscreenButton.onAdd() instanceof Element).toBe(true);
      });

      it('returns a button element', function() {
        expect(element.nodeName.toLowerCase()).toEqual('button');
      });

      it('returns an element with a "fullscreen-button" class', function() {
        expect(element.textContent).toEqual('Open map');
      });

      it('fires a click event when the button is clicked', function() {
        spyOnSignal(fullscreenButton.events.click);
        event.trigger(element, 'click');

        expect(fullscreenButton.events.click).toHaveBeenDispatched();
      });

    });

    describe('#focus', function() {

      it('sets focus to the element', function() {
        document.body.focus();
        fullscreenButton.focus();

        expect(document.activeElement).toEqual(element);
      });

    });

  });

});
