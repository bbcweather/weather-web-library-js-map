define([
  'weather/map/map',
  'weather/map/control/filler',
  'weather/map/control/control',
  'weather/map/util/dom'
], function(Map, Filler, Control, dom) {
  'use strict';

  describe('control.filler', function() {

    var filler;

    beforeEach(function() {
      filler = new Filler();
    });

    describe('constructor', function() {

      it ('is an instance of Control', function() {
        expect(filler instanceof Control).toBe(true);
      });

    });

    describe('#onAdd', function() {

      var element;

      beforeEach(function() {
        element = filler.onAdd();
      });

      it('returns an element with a filler class name', function() {
        expect(dom.hasClass(element, 'filler')).toBe(true);
      });

    });

  });

});
