define([
  'weather/map/map',
  'weather/map/control/key_button',
  'weather/map/control/control',
  'weather/map/util/dom',
  'test/helper/event',
  'vendor/jasmine-signals/jasmine-signals'
], function(Map, KeyButton, Control, dom, event, spyOnSignal) {
  'use strict';

  describe('control.key_button', function() {

    var keyButton,
        element;

    beforeEach(function() {
      keyButton = new KeyButton();
      element = keyButton.onAdd();
      document.body.appendChild(element);
    });

    afterEach(function() {
      document.body.removeChild(element);
    });

    describe('constructor', function() {

      it('is an instance of Control', function() {
        expect(keyButton instanceof Control).toBe(true);
      });

    });

    describe('#onAdd', function() {

      it('returns an Element', function() {
        expect(keyButton.onAdd() instanceof Element).toBe(true);
      });

      it('returns a button element', function() {
        expect(element.nodeName.toLowerCase()).toEqual('button');
      });

      it('returns an element with a "key-button" class', function() {
        expect(dom.hasClass(element, 'key-button')).toBe(true);
      });

      it('returns an element with "Open key" text', function() {
        expect(element.textContent).toEqual('Open key');
      });

      it('dispatches the click event when clicked', function() {
        spyOnSignal(keyButton.events.click);
        event.trigger(element, 'click');

        expect(keyButton.events.click).toHaveBeenDispatched();
      });

    });

    describe('#focus', function() {

      it('gives the element focus', function() {
        document.body.focus();
        keyButton.focus();

        expect(document.activeElement).toEqual(element);
      });

    });

  });

});
