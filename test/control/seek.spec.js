define([
  'weather/map/map',
  'weather/map/control/seek',
  'weather/map/control/control',
  'weather/map/util/dom',
  'weather/map/util/browser',
  'test/helper/event',
  'vendor/jasmine-signals/jasmine-signals',
  'signals'
], function(Map, Seek, Control, dom, browser, event, spyOnSignal, signals) {
  'use strict';

  describe('control.seek', function() {

    var seek,
        markers;

    beforeEach(function() {
      markers = [
        { position: 0, label: 'Wed' },
        { position: 30 },
        { position: 60 },
        { position: 90 },
        { position: 120 },
        { position: 150 },
        { position: 180, label: 'Thu' },
        { position: 210 },
        { position: 240 },
        { position: 270 },
        { position: 300 },
        { position: 330 },
        { position: 360 },
        { position: 390 },
        { position: 420, label: 'Fri' },
        { position: 540 },
        { position: 660, label: 'Sat' },
        { position: 780 },
        { position: 900, label: 'Sun' },
        { position: 1000 }
      ];
      seek = new Seek({
        markers: markers,
        duration: 1000
      });
    });

    describe('constructor', function() {

      it('is an instance of Control', function() {
        expect(seek instanceof Control).toBe(true);
      });

      it('sets up the events', function() {
        expect(seek.events).toBeDefined();
      });

      it('sets up a "seeking" event', function() {
        expect(seek.events.seeking instanceof signals.Signal).toBe(true);
      });

      it('stores the markers', function() {
        expect(seek.markers).toEqual(markers);
      });

      it('stores the duration', function() {
        expect(seek.duration).toEqual(1000);
      });
    });

    describe('#onAdd', function() {

      var seekElement;

      beforeEach(function() {
        seekElement = seek.onAdd();
      });

      it('returns an Element', function() {
        seekElement = seek.onAdd();
        expect(seekElement instanceof Element).toBe(true);
      });

      it('returns an element containing the right number of markers', function() {
        var markers = seekElement.querySelectorAll('.seek__marker');
        expect(markers.length).toEqual(20);
      });

      it('returns an element containing the right number of markers with labels', function() {
        var markersWithLabels = seekElement.querySelectorAll('.seek__marker--with-label');
        expect(markersWithLabels.length).toEqual(5);
      });
      
      it('returns elements each containing the short versions of all the labels', function() {
        var days = ['Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        var markersWithLabels = seekElement.querySelectorAll('.seek__marker--with-label');
        for (var i = 0; i < days.length; days++) {
          expect(markersWithLabels[i].innerHTML).toEqual(days[i]);
        }
      });

      it('returns an element containing a slider', function() {
        expect(seekElement.querySelector('.seek__slider')).not.toBe(null);
      });

      it('returns an element containing a seek bar', function() {
        expect(seekElement.querySelector('.seek__seek-bar')).not.toBe(null);
      });

      it('returns an element containing a time-passed block', function() {
        expect(seekElement.querySelector('.seek__time-passed')).not.toBe(null);
      });

      it('adds an event listener for mouse move', function() {
        spyOn(document, 'addEventListener');
        seek.onAdd();
        expect(document.addEventListener).toHaveBeenCalledWith('mousemove', jasmine.any(Function));
      });

      it('adds an event listener for mouse up', function() {
        spyOn(document, 'addEventListener');
        seek.onAdd();
        expect(document.addEventListener).toHaveBeenCalledWith('mouseup', jasmine.any(Function));
      });

      it('adds an event listener for touch move on touch devices', function() {
        spyOn(document, 'addEventListener');
        spyOn(browser, 'hasTouchSupport').and.returnValue(true);
        seek.onAdd();
        expect(document.addEventListener).toHaveBeenCalledWith('touchmove', jasmine.any(Function));
      });

      it('adds an event listener for touch cancel on touch devices', function() {
        spyOn(document, 'addEventListener');
        spyOn(browser, 'hasTouchSupport').and.returnValue(true);
        seek.onAdd();
        expect(document.addEventListener).toHaveBeenCalledWith('touchcancel', jasmine.any(Function));
      });

      it('adds an event listener for touch end on touch devices', function() {
        spyOn(document, 'addEventListener');
        spyOn(browser, 'hasTouchSupport').and.returnValue(true);
        seek.onAdd();
        expect(document.addEventListener).toHaveBeenCalledWith('touchend', jasmine.any(Function));
      });

      it('does not add an event listener for touch move on non-touch devices', function() {
        spyOn(document, 'addEventListener');
        spyOn(browser, 'hasTouchSupport').and.returnValue(false);
        seek.onAdd();
        expect(document.addEventListener).not.toHaveBeenCalledWith('touchmove', jasmine.any(Function));
      });

      it('does not add an event listener for touch cancel on touch non-devices', function() {
        spyOn(document, 'addEventListener');
        spyOn(browser, 'hasTouchSupport').and.returnValue(false);
        seek.onAdd();
        expect(document.addEventListener).not.toHaveBeenCalledWith('touchcancel', jasmine.any(Function));
      });

      it('does not add an event listener for touch end on non-touch devices', function() {
        spyOn(document, 'addEventListener');
        spyOn(browser, 'hasTouchSupport').and.returnValue(false);
        seek.onAdd();
        expect(document.addEventListener).not.toHaveBeenCalledWith('touchend', jasmine.any(Function));
      });

      it('dispatches the seeking event when mouse button is pressed down over it', function() {
        spyOnSignal(seek.events.seeking);

        event.trigger(seekElement, 'mousedown');
        expect(seek.events.seeking).toHaveBeenDispatched();
      });

      it('dispatches the seeking event when mouse is dragged across seek bar', function() {
        spyOnSignal(seek.events.seeking);

        event.trigger(seekElement, 'mousedown');
        event.trigger(document, 'mousemove');
        expect(seek.events.seeking).toHaveBeenDispatched();
      });

      it('does not dispatch the seeking event when the mouse is moved without pressing the mouse button down', function() {
        spyOnSignal(seek.events.seeking);

        event.trigger(document, 'mousemove');
        expect(seek.events.seeking).not.toHaveBeenDispatched();
      });

      it('does not dispatch the seeking event when the mouse is moved if the seek bar was not the origin of the drag', function() {
        spyOnSignal(seek.events.seeking);

        event.trigger(document, 'mousedown');
        event.trigger(document, 'mousemove');
        expect(seek.events.seeking).not.toHaveBeenDispatched();
      });

      it('does not dispatch the seeking event when the mouse is moved if the mouse button is no longer pressed down', function() {
        event.trigger(seekElement, 'mousedown');
        event.trigger(document, 'mousemove');

        spyOnSignal(seek.events.seeking);

        event.trigger(document, 'mouseup');
        event.trigger(document, 'mousemove');
        expect(seek.events.seeking).not.toHaveBeenDispatched();
      });

    });

    describe('#setPosition', function() {

      it('updates the width of the time passed div', function() {
        seek.onAdd();
        var markerPositionMappings = [0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 54, 66, 78, 90, 100];
        for (var i = 0; i < markers.length; i++) {
          seek.setPosition(markers[i].position);
          expect(seek.timePassed.style.width).toEqual(markerPositionMappings[i] + '%');
        }
      });

    });

  });

});
