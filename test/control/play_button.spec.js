define([
  'weather/map/control/play_button',
  'weather/map/control/control',
  'weather/map/util/dom',
  'test/helper/event',
  'vendor/jasmine-signals/jasmine-signals'
], function(PlayButton, Control, dom, event, spyOnSignal) {
  'use strict';

  describe('control.play', function() {

    var playButton;

    beforeEach(function() {
      playButton = new PlayButton();
    });

    describe('constructor', function() {

      it('is an instance of Control', function() {
        expect(playButton instanceof Control).toBe(true);
      });

      it('it sets up the events', function() {
        expect(playButton.events).toBeDefined();
      });

    });

    describe('#onAdd', function() {

      it('returns an Element', function() {
        expect(playButton.onAdd() instanceof Element).toBe(true);
      });

      it('returns an Element with the expected html markup', function() {
        var html = '<button class="play-button">' +
          '<span class="play-button__label">Play</span>' +
          '</button>';

        expect(playButton.onAdd().outerHTML).toEqual(html);
      });

      it('emits the play, replay and pause click events and changes the icon when clicked', function() {
        var playButtonEl = playButton.onAdd();

        spyOnSignal(playButton.events.playClick);
        spyOn(playButton, '_showPause').and.callThrough();

        event.trigger(playButtonEl, 'click');

        expect(playButton.events.playClick).toHaveBeenDispatched();
        expect(playButton._showPause).toHaveBeenCalled();

        spyOnSignal(playButton.events.pauseClick);
        spyOn(playButton, 'showPlay').and.callThrough();

        event.trigger(playButtonEl, 'click');

        expect(playButton.events.pauseClick).toHaveBeenDispatched();
        expect(playButton.showPlay).toHaveBeenCalled();

        spyOnSignal(playButton.events.replayClick);
        playButton._showPause.calls.reset();

        playButton.showReplay();
        event.trigger(playButtonEl, 'click');

        expect(playButton.events.replayClick).toHaveBeenDispatched();
        expect(playButton._showPause).toHaveBeenCalled();
      });

    });

    describe('state change', function() {

      var playButtonEl, playLabel;

      beforeEach(function() {
        playButtonEl = playButton.onAdd();
        playLabel = playButtonEl.querySelector('.play-button__label');
      });

      describe('#_showPause', function() {

        it('updates the button with the Pause icon and label', function() {
          playButton._showPause();

          expect(playLabel.innerHTML).toBe('Pause');
          expect(dom.hasClass(playButtonEl, 'play-button--pause')).toBe(true);
          expect(dom.hasClass(playButtonEl, 'play-button--replay')).toBe(false);
        });

      });

      describe('#showPlay', function() {

        it('updates the button with the Play icon and label', function() {
          playButton._showPause();
          playButton.showPlay();

          expect(playLabel.innerHTML).toBe('Play');
          expect(dom.hasClass(playButtonEl, 'play-button--pause')).toBe(false);
          expect(dom.hasClass(playButtonEl, 'play-button--replay')).toBe(false);
        });

      });

      describe('#showReplay', function() {

        it( 'updates the button with the Replay icon and label', function() {
          playButton.showReplay();

          expect(playLabel.innerHTML).toBe('Replay');
          expect(dom.hasClass(playButtonEl, 'play-button--replay')).toBe(true);
          expect(dom.hasClass(playButtonEl, 'play-button--pause')).toBe(false);
        });

      });

    });

  });

});
