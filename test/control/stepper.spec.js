define([
  'weather/map/map',
  'weather/map/control/stepper',
  'weather/map/control/control',
  'weather/map/util/dom',
  'test/helper/event',
  'vendor/jasmine-signals/jasmine-signals',
  'signals'
], function(Map, Stepper, Control, dom, event, spyOnSignal, signals) {
  'use strict';

  describe('control.stepper', function() {

    var stepper,
        stepperElement,
        keyCodes;

    keyCodes = {
      left: 37,
      right: 39
    };

    beforeEach(function() {
      stepper = new Stepper();
      stepperElement = stepper.onAdd();
      document.body.appendChild(stepperElement);
    });

    afterEach(function() {
      document.body.removeChild(stepperElement);
    });

    describe('constructor', function() {

      it('is an instance of Control', function() {
        expect(stepper instanceof Control).toBe(true);
      });

      it('sets up the events', function() {
        expect(stepper.events).toBeDefined();
      });

      it('sets up a "next" event', function() {
        expect(stepper.events.next instanceof signals.Signal).toBe(true);
      });

      it('sets up a "previous" event', function() {
        expect(stepper.events.previous instanceof signals.Signal).toBe(true);
      });

    });

    describe('#onAdd', function() {

      it('returns an Element', function() {
        expect(stepper.onAdd() instanceof Element).toBe(true);
      });

      it('returns an element containing a previous button', function() {
        var button = stepperElement.querySelector('.stepper__button--previous');

        expect(button).not.toBe(null);
      });

      it('returns an element containing a next button', function() {
        var button = stepperElement.querySelector('.stepper__button--next');

        expect(button).not.toBe(null);
      });

      it('emits a previous event when the previous button is clicked', function() {
        spyOnSignal(stepper.events.previous);
        event.trigger(stepper.previousButton, 'click');

        expect(stepper.events.previous).toHaveBeenDispatched();
      });

      it('emits a next event when the next button is clicked', function() {
        spyOnSignal(stepper.events.next);
        event.trigger(stepper.nextButton, 'click');

        expect(stepper.events.next).toHaveBeenDispatched();
      });

      it('removes focus from the element when mouse moves off next button', function() {
        stepper.nextButton.focus();
        event.trigger(stepper.nextButton, 'mouseout');

        expect(document.activeElement).not.toEqual(stepper.nextButton);
      });

      it('removes focus from the element when mouse moves off the previous button', function() {
        stepper.previousButton.focus();
        event.trigger(stepper.previousButton, 'mouseout');

        expect(document.activeElement).not.toEqual(stepper.previousButton);
      });

      describe('pressing the right arrow key', function() {

        beforeEach(function() {
          stepper.previousButton.focus();
          spyOnSignal(stepper.events.next);
          event.trigger(stepperElement, 'keydown', { keyCode: keyCodes.right });
        });

        it('emits a next event', function() {
          expect(stepper.events.next).toHaveBeenDispatched();
        });

        it('sets focus to the next button', function() {
          expect(document.activeElement).toEqual(stepper.nextButton);
        });

      });

      describe('pressing the left arrow key', function() {

        beforeEach(function() {
          stepper.nextButton.focus();
          spyOnSignal(stepper.events.previous);
          event.trigger(stepperElement, 'keydown', { keyCode: keyCodes.left });
        });

        it('emits a previous event when the left arrow button is pressed', function() {
          expect(stepper.events.previous).toHaveBeenDispatched();
        });

        it ('sets focus to the previous button', function() {
          expect(document.activeElement).toEqual(stepper.previousButton);
        });

      });

    });

    describe('#hasNext', function() {

      it('disables the next button when set to false', function() {
        stepper.hasNext(false);

        expect(stepper.nextButton.disabled).toBe(true);
      });

      it('enables the next button when set to true', function() {
        stepper.nextButton.disabled = true;
        stepper.hasNext(true);

        expect(stepper.nextButton.disabled).toBe(false);
      });

      it('sets focus on the previous button when set to false', function() {
        stepper.nextButton.focus();
        stepper.hasNext(false);

        expect(document.activeElement).toEqual(stepper.previousButton);
      });

      it('returns true if the next button is not disabled', function() {
        expect(stepper.hasNext()).toBe(true);
      });

      it('returns false if the next button is disabled', function() {
        stepper.hasNext(false);
        expect(stepper.hasNext()).toBe(false);
      });

    });

    describe('#hasPrevious', function() {

      it('disables previous button when set to false', function() {
        stepper.hasPrevious(false);

        expect(stepper.previousButton.disabled).toBe(true);
      });

      it('enables the previous button when set to true', function() {
        stepper.previousButton.disabled = true;
        stepper.hasPrevious(true);

        expect(stepper.previousButton.disabled).toBe(false);
      });

      it('sets focus on the next button when set to false', function() {
        stepper.previousButton.focus();
        stepper.hasPrevious(false);

        expect(document.activeElement).toEqual(stepper.nextButton);
      });

      it('returns true if the previous button is not disabled', function() {
        expect(stepper.hasPrevious()).toBe(true);
      });

      it('returns false if the previous button is disabled', function() {
        stepper.hasPrevious(false);
        expect(stepper.hasPrevious()).toBe(false);
      });

    });

  });

});
