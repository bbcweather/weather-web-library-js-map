define([
  'weather/map/controller/responsive',
  'weather/api',
  'weather/map/util/dom',
  'weather/map/util/browser',
  'test/helper/event'
], function(ResponsiveMap, Api, dom, browser, event) {
  'use strict';

  describe('controller.responsive', function() {

    var controller,
        container,
        api;

    function resetMapContainer() {
      document.body.removeChild(container);
      container = document.createElement('div');
      document.body.appendChild(container);
    }

    beforeEach(function() {
      spyOn(browser, 'hasTouchSupport').and.returnValue(false);

      container = document.createElement('div');
      document.body.appendChild(container);

      api = new Api({
        environment: 'live',
        locale: 'en',
        timezone: 'Europe/London'
      });

      controller = new ResponsiveMap({
        api: api,
        container: container
      });
    });

    afterEach(function() {
      document.body.removeChild(container);
    });

    describe('constructor', function() {

      it('sets up container from options', function() {
        expect(controller.container).toEqual(container);
      });

      it('sets isFullscreen to false', function() {
        expect(controller.isFullscreen).toBe(false);
      });

      it('sets up the map', function() {
        expect(controller.map).toBeDefined();
      });

      it('sets up a fullscreen button control', function() {
        expect(controller.fullscreenButton).toBeDefined();
      });

      it('adds the fullscreen button control to the map', function() {
        expect(controller.map.hasControl(controller.fullscreenButton)).toBe(true);
      });

      it('sets up a close button control', function() {
        expect(controller.closeButton).toBeDefined();
      });

      it('adds the close button control to the map', function() {
        expect(controller.map.hasControl(controller.closeButton)).toBe(true);
      });

      it('sets up a key button control', function() {
        expect(controller.keyButton).toBeDefined();
      });

      it('adds the key button control to the map', function() {
        expect(controller.map.hasControl(controller.keyButton)).toBe(true);
      });

      it('sets up a key panel control', function() {
        expect(controller.keyPanel).toBeDefined();
      });

      it('adds the key panel control to the map', function() {
        expect(controller.map.hasControl(controller.keyPanel)).toBe(true);
      });

      it('adds a no touch class to the container', function() {
        expect(dom.hasClass(controller.container, 'weather-map--no-touch')).toBe(true);
      });

      it('does not add a no touch class to the container if the browser supports touch', function() {
        browser.hasTouchSupport.and.returnValue(true);
        document.body.removeChild(container);
        container = document.createElement('div');
        document.body.appendChild(container);
        controller = new ResponsiveMap({
          api: api,
          container: container
        });

        expect(dom.hasClass(controller.container, 'weather-map--no-touch')).toBe(false);
      });

      it('opens the map when the fullscreen button control is clicked', function() {
        var fullscreenButton;

        spyOn(controller, 'open');
        fullscreenButton = controller.map.controlsContainer.querySelector('.fullscreen-button');
        event.trigger(fullscreenButton, 'click');

        expect(controller.open).toHaveBeenCalled();
      });

      it('closes the map when the close button control is clicked', function() {
        var closeButton;

        spyOn(controller, 'close');
        closeButton = controller.map.controlsContainer.querySelector('.close-button');
        event.trigger(closeButton, 'click');

        expect(controller.close).toHaveBeenCalled();
      });

      it('opens the key panel control when the key button is clicked', function() {
        var keyButton;

        spyOn(controller.keyPanel, 'open');
        keyButton = controller.map.controlsContainer.querySelector('.key-button');
        event.trigger(keyButton, 'click');

        expect(controller.keyPanel.open).toHaveBeenCalled();
      });

      it('sets focus to the key button when the key panel is closed', function() {
        spyOn(controller.keyButton, 'focus');
        controller.keyPanel.events.close.dispatch();
        expect(controller.keyButton.focus).toHaveBeenCalled();
      });

      it('opens the map when the location hash is changed to map-fullscreen', function(done) {
        var controllerOpenSpy = spyOn(controller, 'open');
        window.addEventListener('hashchange', function() {
          expect(controllerOpenSpy).toHaveBeenCalled();
          done();
        });
        window.location.hash = 'map-fullscreen';
      });

      it('closes the map when the location hash is changed to map-fullscreen', function(done) {
        controller.open();

        var controllerCloseSpy = spyOn(controller, 'close');

        window.addEventListener('hashchange', function() {
          expect(controllerCloseSpy).toHaveBeenCalled();
          done();
        });
        window.location.hash = '';
      });

      it('defaults the mode to auto', function() {
        expect(controller.mode).toEqual(ResponsiveMap.modes.auto);
      });

      it('sets the mode to the options', function() {
        resetMapContainer();
        controller = new ResponsiveMap({
          api: api,
          container: container,
          mode: ResponsiveMap.modes.teaser
        });
        expect(controller.mode).toEqual(ResponsiveMap.modes.teaser);
      });

    });

    describe('#open', function() {

      it('activates the map', function() {
        spyOn(controller.map, 'activate');
        controller.open();

        expect(controller.map.activate).toHaveBeenCalled();
      });

      it('adds a fullscreen class to the container', function() {
        controller.open();

        expect(dom.hasClass(controller.container, 'weather-map--fullscreen')).toBe(true);
      });

      it('invalidates the size of the map', function() {
        spyOn(controller.map, 'invalidateSize');
        controller.open();

        expect(controller.map.invalidateSize).toHaveBeenCalled();
      });

      it('sets is fullscreen to true', function() {
        controller.open();

        expect(controller.isFullscreen).toBe(true);
      });

      it('updates the location hash to map-fullscreen', function() {
        controller.open();

        expect(window.location.hash).toEqual('#map-fullscreen');
      });

    });

    describe('#close', function() {

      describe('mode: auto', function() {

        it('removes the fullscreen class from the container', function() {
          dom.addClass(controller.container, 'weather-map--fullscreen');
          controller.close();

          expect(dom.hasClass(controller.container, 'weather-map--fullscreen')).toBe(false);
        });

        it('sets is fullscreen to false', function() {
          controller.isFullscreen = true;
          controller.close();

          expect(controller.isFullscreen).toBe(false);
        });

        it('invalidates the size of the map', function() {
          spyOn(controller.map, 'invalidateSize');
          controller.close();

          expect(controller.map.invalidateSize).toHaveBeenCalled();
        });

        it('resets the map', function() {
          spyOn(controller.map, 'reset');
          controller.close();

          expect(controller.map.reset).toHaveBeenCalled();
        });

        it('clears the location hash', function() {
          controller.close();

          expect(window.location.hash).toEqual('');
        });

        describe('with a desktop screen width', function() {

          beforeEach(function() {
            controller.desktopMediaQuery = { matches: true };
            controller.mode = ResponsiveMap.modes.auto;
          });

          it('adds a desktop class if the desktop media query matches on a non-touch device', function() {
            controller.close();

            expect(dom.hasClass(controller.container, 'weather-map--desktop')).toBe(true);
          });

          it('does not add a desktop class if the desktop media query matches on a touch device', function() {
            // browser.hasTouchSupport is already a spy at this point so modify return value
            browser.hasTouchSupport.and.returnValue(true);
            controller.close();

            expect(dom.hasClass(controller.container, 'weather-map--desktop')).toBe(false);
          });

        });

        describe('with a screen width below desktop size', function() {

          beforeEach(function() {
            controller.desktopMediaQuery = { matches: false };
          });

          it('deactivates the map', function() {
            spyOn(controller.map, 'deactivate');
            controller.close();

            expect(controller.map.deactivate).toHaveBeenCalled();
          });

          it('resets the map', function() {
            spyOn(controller.map, 'reset');
            controller.close();

            expect(controller.map.reset).toHaveBeenCalled();
          });

          it('does not add a desktop class', function() {
            controller.close();

            expect(dom.hasClass(controller.container, 'weather-map--desktop')).toBe(false);
          });

          it('does not add a touch class', function() {
            controller.isTouchDevice = true;
            controller.close();

            expect(dom.hasClass(controller.container, 'weather-map--touch')).toBe(false);
          });

        });

      });

      describe('mode: teaser', function() {

        beforeEach(function() {
          resetMapContainer();
          controller = new ResponsiveMap({
            api: api,
            container: container,
            mode: ResponsiveMap.modes.teaser
          });
          controller.desktopMediaQuery = { matches: false };
          controller.isTouchDevice = true;
        });

        it('removes the fullscreen class from the container', function() {
          dom.addClass(controller.container, 'weather-map--fullscreen');
          controller.close();

          expect(dom.hasClass(controller.container, 'weather-map--fullscreen')).toBe(false);
        });

        it('sets is fullscreen to false', function() {
          controller.isFullscreen = true;
          controller.close();

          expect(controller.isFullscreen).toBe(false);
        });

        it('invalidates the size of the map', function() {
          spyOn(controller.map, 'invalidateSize');
          controller.close();

          expect(controller.map.invalidateSize).toHaveBeenCalled();
        });

        it('clears the location hash', function() {
          controller.close();

          expect(window.location.hash).toEqual('');
        });

        it('deactivates the map', function() {
          spyOn(controller.map, 'deactivate');
          controller.close();

          expect(controller.map.deactivate).toHaveBeenCalled();
        });

        it('resets the map', function() {
          spyOn(controller.map, 'reset');
          controller.close();

          expect(controller.map.reset).toHaveBeenCalled();
        });

        it('does not add a desktop class', function() {
          controller.close();

          expect(dom.hasClass(controller.container, 'weather-map--desktop')).toBe(false);
        });

      });

    });

  });

});
