define([
  'weather/map/marker/location',
  'weather/map/map',
  'weather/map/util/dom',
  'vendor/leaflet/leaflet'
], function(Location, Map, dom, leaflet) {
  'use strict';

  describe('marker.location', function() {

    var location, locationData, map, container;

    beforeEach(function() {
      container = document.createElement('div');
      document.body.appendChild(container);

      map = new Map({
        container: container
      });

      locationData = {
        id: 2655603,
        name: 'Birmingham',
        coordinates: [0.001, 51.5072],
        forecasts: {
          15033000: {
            temperature: {
              c: 1,
              f: 34
            }
          },
          15033003: {
            temperature: {
              c: 0,
              f: 39
            }
          },
          15033006: {
            temperature: { }
          },
          15033009: { },
          15033013: {
            temperature: {
              c: null,
              f: null
            }
          }
        }
      };

      location = new Location(locationData, { timeslot: 15033000 });
      map.addMarker(location);
    });

    afterEach(function() {
      document.body.removeChild(container);
    });

    describe('constructor', function() {

      it('sets the temperature unit to the option', function() {
        location = new Location(locationData, { temperatureUnit: 'c' });
        expect(location.temperatureUnit).toEqual('c');

        location = new Location(locationData, { temperatureUnit: 'f' });
        expect(location.temperatureUnit).toEqual('f');
      });

      it('defaults the temperature unit to c', function() {
        expect(location.temperatureUnit).toEqual('c');
      });

      it('sets the internal values to the options', function() {
        expect(location.timeslot).toEqual(15033000);
        expect(location.data).toEqual(locationData);
      });

      it('sets the latlng', function() {
        expect(location.latlng).toEqual({ lat: 51.5072, lng: 0.001 });
      });

    });

    describe('#onAdd', function() {

      it('returns a DivIcon', function() {
        expect(location.onAdd() instanceof leaflet.DivIcon).toBe(true);
      });

      it('returns a DivIcon with a location marker class', function() {
        var element = location.onAdd();

        expect(element.options.className).toEqual('weather-map__location-marker');
      });

      it('returns an element with the point class to mark the precise location', function() {
        expect(document.querySelector('.location-marker__point')).not.toBeNull();
      });

      it('returns an element with the location class containing the location', function() {
        expect(document.querySelector('.location-marker__location').innerHTML).toEqual('Birmingham');
      });

      it('returns an element with the temperature class containing the temperature', function() {
        expect(document.querySelector('.location-marker__temperature').innerHTML).toEqual('1');
      });

      it('returns an element with the unique location ID class', function() {
        expect(document.querySelector('.location-marker__temperature-2655603')).not.toBeNull();
      });

      it('returns an element with the relevant temperature colour class', function() {
        expect(document.querySelector('.temperature-1-3')).not.toBeNull();
      });

    });

    describe('#updateTimeslot', function() {

      it('updates the temperature value in the marker when temperatureUnit is set to c', function() {
        location.updateTimeslot(15033000);
        expect(document.querySelector('.location-marker__temperature-2655603').innerHTML).toEqual('1');
      });

      it('updates the temperature value in the marker when temperatureUnit is set to f', function() {
        location = new Location(locationData, { temperatureUnit: 'f', timeslot: 15033000 });
        map.addMarker(location);
        location.updateTimeslot(15033003);
        expect(document.querySelector('.location-marker__temperature-2655603').innerHTML).toEqual('39');
      });

      it('updates the temperature value in the marker when temperature is 0', function() {
        location = new Location(locationData, { timeslot: 15033000 });
        map.addMarker(location);
        location.updateTimeslot(15033003);
        expect(document.querySelector('.location-marker__temperature-2655603').innerHTML).toEqual('0');
      });

      it('updates the temperature value in the marker to a dash when temperature in the relevant unit is undefined', function() {
        location.updateTimeslot(15033006);
        expect(document.querySelector('.location-marker__temperature-2655603').innerHTML).toEqual('-');
      });

      it('updates the temperature value in the marker to a dash when temperature is undefined', function() {
        location.updateTimeslot(15033009);
        expect(document.querySelector('.location-marker__temperature-2655603').innerHTML).toEqual('-');
      });

      it('removes the old temperature class', function() {
        location.updateTimeslot(15033003);
        expect(document.querySelector('.temperature-1-3')).toBeNull();
      });

      it('sets the correct temperature class', function() {
        var temperatureClassMappings, t;

        locationData = {
          name: 'Birmingham',
          coordinates: [0.001, 51.5072],
          forecasts: {
            15033000: { temperature: { c: -2, f: 28 } },
            15033003: { temperature: { c: -1, f: 30 } },
            15033006: { temperature: { c: 0, f: 32 } },
            15033009: { temperature: { c: 1, f: 34 } },
            15033012: { temperature: { c: 2, f: 36 } },
            15033015: { temperature: { c: 3, f: 37 } },
            15033018: { temperature: { c: 4, f: 39 } },
            15033021: { temperature: { c: 5, f: 41 } },
            15033024: { temperature: { c: 6, f: 43 } },
            15033027: { temperature: { c: 7, f: 45 } },
            15033030: { temperature: { c: 8, f: 46 } },
            15033033: { temperature: { c: 9, f: 48 } },
            15033036: { temperature: { c: 10, f: 50 } },
            15033039: { temperature: { c: 11, f: 52 } },
            15033042: { temperature: { c: 12, f: 54 } },
            15033045: { temperature: { c: 13, f: 55 } },
            15033048: { temperature: { c: 14, f: 57 } },
            15033051: { temperature: { c: 15, f: 59 } },
            15033054: { temperature: { c: 16, f: 61 } },
            15033057: { temperature: { c: 17, f: 63 } },
            15033060: { temperature: { c: 18, f: 64 } },
            15033063: { temperature: { c: 19, f: 66 } },
            15033066: { temperature: { c: 20, f: 68 } },
            15033069: { temperature: { c: 21, f: 70 } },
            15033072: { temperature: { c: 22, f: 72 } },
            15033075: { temperature: { c: 23, f: 73 } },
            15033078: { temperature: { c: 24, f: 75 } },
            15033081: { temperature: { c: 25, f: 77 } },
            15033084: { temperature: { c: 26, f: 79 } },
            15033087: { temperature: { c: 27, f: 81 } },
            15033090: { temperature: { c: null, f: null } },
            15033093: { temperature: { } }
          }
        };
        temperatureClassMappings = {
          15033000: 'temperature-0-down',
          15033003: 'temperature-0-down',
          15033006: 'temperature-0-down',
          15033009: 'temperature-1-3',
          15033012: 'temperature-1-3',
          15033015: 'temperature-1-3',
          15033018: 'temperature-4-6',
          15033021: 'temperature-4-6',
          15033024: 'temperature-4-6',
          15033027: 'temperature-7-9',
          15033030: 'temperature-7-9',
          15033033: 'temperature-7-9',
          15033036: 'temperature-10-12',
          15033039: 'temperature-10-12',
          15033042: 'temperature-10-12',
          15033045: 'temperature-13-15',
          15033048: 'temperature-13-15',
          15033051: 'temperature-13-15',
          15033054: 'temperature-16-18',
          15033057: 'temperature-16-18',
          15033060: 'temperature-16-18',
          15033063: 'temperature-19-21',
          15033066: 'temperature-19-21',
          15033069: 'temperature-19-21',
          15033072: 'temperature-22-24',
          15033075: 'temperature-22-24',
          15033078: 'temperature-22-24',
          15033081: 'temperature-25-up',
          15033084: 'temperature-25-up',
          15033087: 'temperature-25-up',
          15033090: 'temperature-na',
          15033093: 'temperature-na'
        };
        for (t in temperatureClassMappings) {
          if (temperatureClassMappings.hasOwnProperty(t)) {
            locationData.id = t; // Set the location ID so that it is unique
            location = new Location(locationData, { temperatureUnit: 'f', timeslot: 15033000 });
            map.addMarker(location);
            location.updateTimeslot(t);
            expect(document.querySelector('.location-marker__temperature-' + t + '.' + temperatureClassMappings[t])).not.toBeNull('Wrong class attached when updating temperature to ' + locationData.forecasts[t].temperature.c);
          }
        }
      });

    });

    describe('#hide', function() {

      it('adds the hidden class to the location marker element', function() {
        location.hide();

        expect(document.querySelector('.weather-map__location-marker--hidden .location-marker__temperature-2655603')).not.toBeNull();
      });

    });

    describe('#isHidden', function() {

      it('returns true when the marker is hidden', function() {
        location.hide();

        expect(location.isHidden()).toBeTruthy();
      });

      it('returns false when the marker is hidden', function() {
        expect(location.isHidden()).toBeFalsy();
      });

    });

  });

});
