define([
  'weather/map/map',
  'weather/map/util/dom',
  'weather/map/layer/tilechef',
  'weather/map/control/close_button',
  'weather/map/marker/location',
  'vendor/leaflet/leaflet',
  'vendor/jasmine-signals/jasmine-signals'
], function(Map, dom, TilechefLayer, CloseButton, LocationMarker, leaflet, spyOnSignal) {
  'use strict';

  describe('map', function() {

    var map,
        container;

    beforeEach(function() {
      container = document.createElement('div');
      document.body.appendChild(container);

      map = new Map({
        container: container
      });
    });

    afterEach(function() {
      document.body.removeChild(container);
    });

    describe('constructor', function() {

      it('throws an error if a container is not passed in options', function() {
        expect(function() { new Map(); }).toThrow();
      });

      it('defaults zoom level to 9 if not set in options', function() {
        expect(map.zoom).toEqual(9);
      });

      it('defaults center to [51.505, -0.09] if not set in options', function() {
        expect(map.center).toEqual([51.505, -0.09]);
      });

      it('sets a container property from the passed in options', function() {
        expect(map.container).toBeDefined();
        expect(map.container).toEqual(container);
      });

      it('adds a class to the container', function() {
        expect(dom.hasClass(map.container, 'weather-map')).toBe(true);
      });

      it('adds the loaded class to the container', function() {
        expect(dom.hasClass(map.container, 'weather-map--loaded')).toBe(true);
      });

      it('adds a no-svg class if the browser does not support svg', function() {
        // This will need updating if the SVG check in util.browser changes
        var originalSVGRect = window.SVGRect;
        window.SVGRect = undefined;

        container.innerHTML = '';
        map = new Map({
          container: container
        });

        expect(dom.hasClass(map.container, 'weather-map--no-svg')).toBe(true);

        window.SVGRect = originalSVGRect;
      });

      it('creates a container for the map', function() {
        var mapContainer = map.container.querySelector('.weather-map__map');
        expect(mapContainer).toBeDefined();
      });

      it('sets an id on the map container', function() {
        var mapContainer = map.container.querySelector('.weather-map__map');
        expect(mapContainer.id).toEqual('leaflet-map');
      });

      it('creates an instance of leaflet', function() {
        expect(map.engine instanceof leaflet.Map).toBe(true);
      });

      it('initializes map engine with the map zoom level', function() {
        expect(map.engine.getZoom()).toEqual(map.zoom);
      });

    });

    describe('#addControl', function() {

      it('calls onAdd on the control', function() {
        var closeButton = new CloseButton();
        spyOn(closeButton, 'onAdd').and.callThrough();
        map.addControl(closeButton);

        expect(closeButton.onAdd).toHaveBeenCalled();
      });

      it('adds the control to the controls array', function() {
        var closeButton = new CloseButton();
        map.addControl(closeButton);
        expect(map.controls.length).toEqual(1);
      });

      it('appends the control to the controls container element', function() {
        var closeButton = new CloseButton();
        expect(map.controlsContainer.children.length).toEqual(0);
        map.addControl(closeButton);
        expect(map.controlsContainer.children.length).toEqual(1);
      });

    });

    describe('#hasControl', function() {

      it('returns false if the control does not exist in the controls array', function() {
        var closeButton = new CloseButton();
        expect(map.hasControl(closeButton)).toBe(false);
      });

      it('returns true if the control exists in the controls array', function() {
        var closeButton = new CloseButton();
        map.addControl(closeButton);

        expect(map.hasControl(closeButton)).toBe(true);
      });

    });

    describe('#addLayer', function() {

      it('adds the layer to the map engine', function() {
        var layer = new TilechefLayer('http://www.example.com');
        map.addLayer(layer);
        expect(map.engine.hasLayer(layer)).toBe(true);
      });

    });

    describe('#getMaxBounds', function() {

      it('returns the UK bounds', function() {
        var ukBounds = leaflet.latLngBounds(
          leaflet.latLng(61.603, -10.854),
          leaflet.latLng(49.022, 2.7125)
        );
        expect(map.getMaxBounds()).toEqual(ukBounds);
      });

    });

    describe('#removeLayer', function() {

      it('removes the layer from the map engine', function() {
        var layer = new TilechefLayer('http://www.example.com');
        spyOn(map.engine, 'removeLayer');
        map.removeLayer(layer);

        expect(map.engine.removeLayer).toHaveBeenCalledWith(layer);
      });

    });

    describe('#addMarker', function() {

      var marker;

      beforeEach(function() {
        marker = new LocationMarker({
          name: 'Greenwich',
          coordinates: [0.001, 51.5072],
          forecasts: {
            15033000: {
              temperature: {
                c: 1,
                f: 34
              }
            }
          }
        },
        {
          timeslot: 15033000
        });
      });

      it('creates the leaflet marker', function() {
        spyOn(leaflet, 'marker').and.callThrough();
        map.addMarker(marker);
        expect(leaflet.marker).toHaveBeenCalled();
      });

      it('adds the leaflet marker to the map', function() {
        var fakeLeafletMarker = { addTo: function() {} };
        spyOn(leaflet, 'marker').and.returnValue(fakeLeafletMarker);
        spyOn(fakeLeafletMarker, 'addTo');
        map.addMarker(marker);
        expect(fakeLeafletMarker.addTo).toHaveBeenCalledWith(map.engine);
      });

      it('disables keyboard interaction with the marker', function() {
        spyOn(leaflet, 'marker').and.callThrough();
        map.addMarker(marker);
        expect(leaflet.marker).toHaveBeenCalledWith(jasmine.any(Object), jasmine.objectContaining({
          keyboard: false
        }));
      });

    });

    describe('#invalidateSize', function() {

      it('calls invalidate size on the map engine', function() {
        spyOn(map.engine, 'invalidateSize');
        map.invalidateSize();

        expect(map.engine.invalidateSize).toHaveBeenCalled();
      });

    });

    describe('#reset', function() {

      it('resets the center and zoom on the map engine', function() {
        spyOn(map.engine, 'setView');
        map.reset();

        expect(map.engine.setView).toHaveBeenCalledWith(map.center, map.zoom, true);
      });

    });

    describe('#activate', function() {

      it('enables the dragging handler', function() {
        spyOn(map.engine.dragging, 'enable');
        map.activate();

        expect(map.engine.dragging.enable).toHaveBeenCalled();
      });

      it('enables the keyboard handler', function() {
        spyOn(map.engine.keyboard, 'enable');
        map.activate();

        expect(map.engine.keyboard.enable).toHaveBeenCalled();
      });

      it('adds an active class to the map container', function() {
        map.activate();
        expect(dom.hasClass(map.container, 'weather-map--active')).toBe(true);
      });

      it('fires an active event', function() {
        spyOnSignal(map.events.active);
        map.activate();

        expect(map.events.active).toHaveBeenDispatched();
      });

      it('adds the map container to the tab index', function() {
        map.activate();

        expect(map.mapContainer.tabIndex).toEqual(0);
      });

      describe('with touch', function() {

        beforeEach(function() {
          // The tap handler is only added if leaflet detects the browser supports
          // touch, phantomjs does not support touch
          map.engine.addHandler('tap', leaflet.Map.Tap);
        });

        it('enables the tap handler', function() {
          spyOn(map.engine.tap, 'enable');
          map.activate();

          expect(map.engine.tap.enable).toHaveBeenCalled();
        });

      });

    });

    describe('#deactivate', function() {

      it('disables the dragging handler', function() {
        spyOn(map.engine.dragging, 'disable');
        map.deactivate();

        expect(map.engine.dragging.disable).toHaveBeenCalled();
      });

      it('disables the keyboard handler', function() {
        spyOn(map.engine.keyboard, 'disable');
        map.deactivate();

        expect(map.engine.keyboard.disable).toHaveBeenCalled();
      });

      it('removes the active class on the map container', function() {
        map.deactivate();

        expect(dom.hasClass(map.container, 'active')).toBe(false);
      });

      it('removes the map container from the tab index', function() {
        map.deactivate();

        expect(map.mapContainer.tabIndex).toEqual(-1);
      });

      it('fires an inactive event', function() {
        spyOnSignal(map.events.inactive);
        map.deactivate();

        expect(map.events.inactive).toHaveBeenDispatched();
      });

      describe('with touch', function() {

        beforeEach(function() {
          // The tap handler is only added if leaflet detects the browser supports
          // touch, phantomjs does not support touch
          map.engine.addHandler('tap', leaflet.Map.Tap);
        });

        it('disables the tap handler', function() {
          spyOn(map.engine.tap, 'disable');
          map.deactivate();

          expect(map.engine.tap.disable).toHaveBeenCalled();
        });

      });

    });

  });

});
