define([
  'weather/map/util/geo'
], function(geo) {

  describe('util.geo', function() {

    describe('#quadKeyFromXYZ', function() {

      describe('zoom level 1', function() {

        var zoom = 1;

        /*
         * At zoom level 1 the world is divided into 4 tiles:
         * 0 = North America
         * 1 = Europe / Asia
         * 2 = South America
         * 3 = Australia
         *
         * Representation of world broken into 4 tiles:
         *
         *        0        1
         *     ------- --------
         *    |       |        |
         *  0 |   0   |    1   |
         *    |       |        |
         *     ------- --------
         *    |       |        |
         *  1 |   2   |    3   |
         *    |       |        |
         *     ------- --------
         *
         * Zoom level to rows / columns is calculated as 2^Zoom (2 to the power of zoom level)
         * Total tiles is calculated as (2^Zoom) * (2^Zoom)
         *
         * Rows / Columns = 2^1 = 2 (0..1)
         * Total tiles = (2^1) * (2^1) = 2 * 2 = 4
         */

        it('returns "0" for the top left tile', function() {
          expect(geo.quadKeyFromXYZ(0, 0, zoom)).toEqual('0');
        });

        it('returns "1" for the top right tile', function() {
          expect(geo.quadKeyFromXYZ(1, 0, zoom)).toEqual('1');
        });

        it('returns "2" for the bottom left tile', function() {
          expect(geo.quadKeyFromXYZ(0, 1, zoom)).toEqual('2');
        });

        it('returns "3" for the bottom right tile', function() {
          expect(geo.quadKeyFromXYZ(1, 1, zoom)).toEqual('3');
        });

      });

      describe('zoom level 2', function() {

        var zoom = 2;

        /*
         * At zoom level 2 the world is further divided into 16 tiles.
         *
         * Representation of world broken into 16 tiles:
         *
         *       0    1    2    3
         *     ---- ---- ---- ----
         *  0 | 00 | 01 | 10 | 11 |
         *     ----0---- ----1----
         *  1 | 02 | 03 | 12 | 13 |
         *     ---- ---- ---- ----
         *  2 | 20 | 21 | 30 | 31 |
         *     ----2---- ----3----
         *  3 | 22 | 23 | 32 | 33 |
         *     ---- ---- ---- ----
         *
         * Rows / columns = 2^2 = 4 (0..3)
         * Total tiles = (2^2) * (2^2) = 4 * 4 = 16
         */

        it('returns "00" for the top left tile', function() {
          expect(geo.quadKeyFromXYZ(0, 0, zoom)).toEqual('00');
        });

        it('returns "11" for the top right tile', function() {
          expect(geo.quadKeyFromXYZ(3, 0, zoom)).toEqual('11');
        });

        it('returns "22" for the bottom left tile', function() {
          expect(geo.quadKeyFromXYZ(0, 3, zoom)).toEqual('22');
        });

        it('returns "33" for the bottom right tile', function() {
          expect(geo.quadKeyFromXYZ(3, 3, zoom)).toEqual('33');
        });

      });

      describe('zoom level 3', function() {

        var zoom = 3;

        /*
         * At zoom level 3 the world is divided into 64 tiles.
         *
         * Representation of tile 00 from zoom level 2 broken into four tiles:
         *
         *        0       1
         *     ------- --------
         *    |       |        |
         *  0 |  000  |  001   |
         *    |       |        |
         *     ------00--------
         *    |       |        |
         *  1 |  002  |  003   |
         *    |       |        |
         *     ------- --------
         *
         * Rows / columns = 2^3 = 8 (0..7)
         * Total tiles = (2^3) * (2^3) = 8 * 8 = 64
         */

        it('returns "000" for the top left tile', function() {
          expect(geo.quadKeyFromXYZ(0, 0, zoom)).toEqual('000');
        });

        it('returns "111" for the top right tile', function() {
          expect(geo.quadKeyFromXYZ(7, 0, zoom)).toEqual('111');
        });

        it('returns "222" for the bottom left tile', function() {
          expect(geo.quadKeyFromXYZ(0, 7, zoom)).toEqual('222');
        });

        it('returns "333" for the bottom right tile', function() {
          expect(geo.quadKeyFromXYZ(7, 7, zoom)).toEqual('333');
        });

      });

      describe('zoom level 9', function() {

        var zoom = 9;

        /*
         * At zoom level 9:
         * Rows / columns = 2^9 = 512 (0..511)
         * Total tiles = (2^9) * (2^9) = 512 * 512 = 262,144
         */

        it('returns "000000000" for the top left tile', function() {
          expect(geo.quadKeyFromXYZ(0, 0, zoom)).toEqual('000000000');
        });

        it('returns "111111111" for the top right tile', function() {
          expect(geo.quadKeyFromXYZ(511, 0, zoom)).toEqual('111111111');
        });

        it('returns "222222222" for the bottom left tile', function() {
          expect(geo.quadKeyFromXYZ(0, 511, zoom)).toEqual('222222222');
        });

        it('returns "333333333" for the bottom right tile', function() {
          expect(geo.quadKeyFromXYZ(511, 511, zoom)).toEqual('333333333');
        });

        // Test around the centre point 255/256

        it('returns "033333333" for the tile top left of centre', function() {
          expect(geo.quadKeyFromXYZ(255, 255, zoom)).toEqual('033333333');
        });

        it('returns "122222222" for the tile top right of centre', function() {
          expect(geo.quadKeyFromXYZ(256, 255, zoom)).toEqual('122222222');
        });

        it('returns "211111111" for the tile bottom left of centre', function() {
          expect(geo.quadKeyFromXYZ(255, 256, zoom)).toEqual('211111111');
        });

        it('returns "300000000" for the tile bottom right of centre', function() {
          expect(geo.quadKeyFromXYZ(256, 256, zoom)).toEqual('300000000');
        });

      });

    });

  });

});
