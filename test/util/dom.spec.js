define([
  'weather/map/util/dom'
], function(dom) {

  describe('dom.geo', function() {

    describe('#create', function() {

      var element;

      it('returns the element', function() {
        element = dom.create('div');
        expect(element.nodeName.toLowerCase()).toEqual('div');
      });

      it('allows not specifying a class name', function() {
        element = dom.create('div');
        expect(element.className).toEqual('');
      });

      it('sets the class name', function() {
        element = dom.create('div', 'test-class');
        expect(element.className).toEqual('test-class');
      });

      it('appends the element to the supplied container', function() {
        var container = dom.create('div');
        element = dom.create('button', undefined, container);
        expect(container.outerHTML).toEqual('<div><button></button></div>');
      });

    });

    describe('#hasClass', function() {

      var element;

      beforeEach(function() {
        element = dom.create('div', 'test-class');
      });

      it('returns true if the class exists', function() {
        expect(dom.hasClass(element, 'test-class')).toBe(true);
      });

      it('returns false if the class does not exist', function() {
        expect(dom.hasClass(element, 'not-a-class')).toBe(false);
      });

      it('does not return true for partial class name matches', function() {
        expect(dom.hasClass(element, 'test')).toBe(false);
      });

    });

    describe('#addClass', function() {

      var element;

      beforeEach(function() {
        element = dom.create('div');
      });

      it('adds a class to the element', function() {
        dom.addClass(element, 'test-class');
        expect(element.className).toEqual('test-class');
      });

      it('can add multiple classes', function() {
        dom.addClass(element, 'first-class');
        dom.addClass(element, 'second-class');
        expect(element.className).toEqual('first-class second-class');
      });

      it('preserves existing classes', function() {
        element.className = 'existing-class';
        dom.addClass(element, 'new-class');
        expect(element.className).toEqual('existing-class new-class');
      });

      it('does not add multiple classes of the same name', function() {
        dom.addClass(element, 'test-class');
        dom.addClass(element, 'test-class');
        expect(element.className).toEqual('test-class');
      });

    });

    describe('#removeClass', function() {

      var element;

      beforeEach(function() {
        element = dom.create('div', 'test-class');
      });

      it('removes the class from the element', function() {
        dom.removeClass(element, 'test-class');
        expect(element.className).toEqual('');
      });

      it('preserves existing classes', function() {
        element.className = 'preserve-class test-class';
        dom.removeClass(element, 'test-class');
        expect(element.className).toEqual('preserve-class');
      });

    });

  });

});
