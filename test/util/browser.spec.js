define(['weather/map/util/browser'], function(browser) {

  describe('util.browser', function() {

    describe('#hasSvgSupport', function() {

      it('returns true if the browser supports SVG', function() {
        // PhantomJS supports SVG so just assert true
        expect(browser.hasSvgSupport()).toBe(true);
      });

      it('returns false if the browser does not support SVG', function() {
        var originalSVGRect = window.SVGRect;
        window.SVGRect = undefined;

        expect(browser.hasSvgSupport()).toBe(false);

        window.SVGRect = originalSVGRect;
      });

    });

  });

});
