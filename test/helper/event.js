define(function() {

  return {

    /**
     * Trigger an event on an element with optional event properties.
     * @param {HTMLDOMElement} el
     * @param {string} name - e.g. 'click'
     * @param {object} props - optional set of properties to be added to the event
     */
    trigger: function(el, name, props) {
      var e = document.createEvent('HTMLEvents'),
          _props = props || {},
          property;

      e.initEvent(name, true, true);

      for (property in _props) {
        if (props.hasOwnProperty(property)) {
          e[property] = _props[property];
        }
      }

      el.dispatchEvent(e);
    }

  };

});
