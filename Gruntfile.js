module.exports = function(grunt) {
  'use strict';

  require('time-grunt')(grunt);

  // Prevent grunt from autoloading templates to avoid an error message
  // see https://github.com/maenu/grunt-template-jasmine-istanbul/issues/8
  require('load-grunt-tasks')(grunt, {
    pattern: ['grunt-*', '!grunt-template-jasmine-requirejs']
  });

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    config: grunt.file.readJSON('./tasks/config.json'),
    clean: require('./tasks/clean')(grunt),
    bowercopy: require('./tasks/bowercopy')(grunt),
    jshint: require('./tasks/jshint')(grunt),
    jscs: require('./tasks/jscs')(grunt),
    jasmine: require('./tasks/jasmine')(grunt),
    connect: require('./tasks/connect')(grunt),
    requirejs: require('./tasks/requirejs')(grunt),
    copy: require('./tasks/copy')(grunt),
    sass: require('./tasks/sass')(grunt),
    svgmin: require('./tasks/svgmin')(grunt),
    grunticon: require('./tasks/grunticon')(grunt),
    replace: require('./tasks/replace')(grunt),
    watch: require('./tasks/watch')(grunt)
  });

  grunt.registerTask('init', ['bowercopy', 'scss']);
  grunt.registerTask('lint', ['jshint', 'jscs']);
  grunt.registerTask('test', ['lint', 'jasmine']);
  grunt.registerTask('serve', ['connect:server']);
  grunt.registerTask('scss', ['svgmin', 'grunticon:icons', 'replace:icons', 'sass', 'clean:grunticon']);
  grunt.registerTask('dist', ['clean', 'init', 'requirejs']);

  grunt.registerTask('default', ['clean', 'test']);

};
