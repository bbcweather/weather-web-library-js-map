Changes
=======

0.0.11 - 2015-05-06
-------------------
* Fixed fullscreen CSS issue
* Prevented map from attempting to load tiles that are outside the bounds


0.0.10 - 2015-05-01
-------------------
* Added play button
* Added drag bar control
* Added Welsh
* Added left/right arrow key support
* Accessibility Improvements
* Performance Improvements
* Fix bug displaying null temperature values
* Fix bug where IE9 was missing locations
* Fixed bug where centre of the map loaded in the wrong position on some devices
* Fixed bug where closing a map when viewing a location near the edge of the bounds causes the map to fail to reset to the center
* Fix voiceover for stepper buttons on iOS8
* Make day/time label selectable for voiceover
* Prevent drag starts from day/time label
* All devices now start with the CTA


0.0.9 - 2015-04-13
------------------
* Moved the controller to controller/responsive
* Added temperature unit handling
* Changed first timeslot to be the timeslot denoted as initial by the service layer
* Added showing the active location on the map
* Added support for a lack of temperature data
* Added button colour changing on focus and on hover
* Prevented map from consuming mouse wheel scroll event
* Changed day/time label to use translations and time (in the correct timezone) from the service layer
* Changed map to never load as active on touch devices
* More specific text inside Close buttons
* Fixed only one location showing on some devices
* Fixed stepper buttons layout on some devices
* Changed API version to 0.0.2
* Fixed dragging cusor
* Fixed cursor when hovering over hidden key panel button
* Browser/device back and then forward buttons close and open the map


0.0.8 - 2015-04-01
------------------
* Added the controller to the compiled export
* Improved CSS class naming


0.0.7 - 2015-03-31
------------------
* Added a 'Responsive Map' controller
* Added use of API module
* Simplified CSS for IE8 handling
* Added Location Markers at default zoom level in visible tiles, hiding overlapping locations
* Added dragging cursor when dragging
* Fixed bug with DayTime label day/night and Sundays
* Added switching between desktop and mobile views
* Added entry/exit animation to Key panel


0.0.6 - 2015-03-18
------------------

* Added full IE8 support
* Added day and time label
* Added map key
* Added stepper controls and temporal layer
* Added svgmin to minify SVGs


0.0.5 - 2015-03-10
------------------

* Added background image as an svg
* Allow accessing control and layer dependencies from base module
* Changed method for displaying map fullscreen to used position fixed


0.0.4 - 2015-03-03
------------------

* Emitting event when opening or closing fullscreen


0.0.3 - 2015-03-03
------------------

* Fixed bug preventing scrolling on touch devices when map is deactivated


0.0.2 - 2015-03-02
------------------

* Added fullscreen and close button controls.
* Added methods to the Map class for toggling fullscreen, disabling interaction and adding controls.
* No longer adding a tilechef layer upon instantiation.


0.0.1 - 2015-02-11
------------------

* Initial release


0.0.0 - 2015-01-29
------------------

* Project creation
